/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import entity.CustomPopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPanel;

/**
 *
 * @author Babu Khan
 */
public class MyMouseListener implements MouseListener {
    
    private final int roomBookingIdFinal;
    private final String choiceFinal;
    private final JPanel panelFinal;

    private MainForm mainForm;
    private  AllReservationsModified allReservationFormModified;
    
    public MyMouseListener(int roomBookingIdFinal, String choiceFinal, JPanel panelFinal, MainForm mainForm, AllReservationsModified allReservationFormModified) {
        this.roomBookingIdFinal = roomBookingIdFinal;
        this.choiceFinal = choiceFinal;
        this.panelFinal = panelFinal;
        this.mainForm = mainForm;
        this.allReservationFormModified = allReservationFormModified;
    }
    
    
    @Override
            public void mouseClicked(MouseEvent e) {
                CustomPopupMenu pop = new CustomPopupMenu(choiceFinal, roomBookingIdFinal, panelFinal, mainForm);
                pop.showPopup(e, allReservationFormModified);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                
            }

            @Override
            public void mouseExited(MouseEvent e) {
                
            }
}
