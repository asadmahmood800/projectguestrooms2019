/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import com.toedter.calendar.JDateChooser;
import entity.RoomBooking;
import entity.Rooms;
import entity.User;
import entity.WaitingList;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import services.CommonService;
import services.RoomBookingServices;
import services.RoomService;
import table_models.RoomBookingTableModel;

/**
 *
 * @author shad9619
 */
public class MakeReservationForm extends javax.swing.JInternalFrame {

    private User user;
    private MainForm mainForm;
    private Rooms room;
    private RoomBooking roomBooking;
    private RoomBookingServices roomBookingServices;
    private Vector<Rooms> vectorRooms;
    private CommonService commonService;
    private Vector<RoomBooking> vectorRoomBooking;
    private WaitingList waitingList;

    public MakeReservationForm(User user, MainForm mainForm) {
        
        this.user = user;
System.out.println(user.getUserId());
        this.mainForm = mainForm;
        initComponents();
        commonService = new CommonService();

        roomBookingServices = new RoomBookingServices();
        vectorRooms = commonService.getVectorRoomNumber();

        DefaultComboBoxModel modelRoomNumber = new DefaultComboBoxModel(vectorRooms);
        cmbRoomNumber.setModel(modelRoomNumber);

        vectorRoomBooking = commonService.getVectorRoomBooking();
        RoomBookingTableModel model = new RoomBookingTableModel(vectorRoomBooking);
        jtMakeReservation.setModel(model);
    }

    public MakeReservationForm(WaitingList waitingList) {
        this.waitingList = waitingList;
        initComponents();
        commonService = new CommonService();

        roomBookingServices = new RoomBookingServices();
        vectorRooms = commonService.getVectorRoomNumber();

        DefaultComboBoxModel modelRoomNumber = new DefaultComboBoxModel(vectorRooms);
        cmbRoomNumber.setModel(modelRoomNumber);

        vectorRoomBooking = commonService.getVectorRoomBooking();
        RoomBookingTableModel model = new RoomBookingTableModel(vectorRoomBooking);
        jtMakeReservation.setModel(model);

        txtCustomerName.setText(waitingList.getCustomerName());
        txtCnic.setText(waitingList.getCnic());
        txtMobileNumber.setText(waitingList.getPhoneNumber());
        txtAddress.setText(waitingList.getAddress());

        String fromDate = waitingList.getFromDate();
        Date datefrom = new Date(fromDate);
        dateChooserFrom.setDate(datefrom);

        String toDate = waitingList.getToDate();
        Date dateto = new Date(toDate);
        dateChooserTo.setDate(dateto);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        MakeReservationMainPanal = new javax.swing.JPanel();
        MakeReservationPanal = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtMobileNumber = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtCustomerName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtAddress = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtCnic = new javax.swing.JTextField();
        btnReserveRoom = new javax.swing.JButton();
        cmbRoomNumber = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        dateChooserFrom = new com.toedter.calendar.JDateChooser();
        dateChooserTo = new com.toedter.calendar.JDateChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtMakeReservation = new javax.swing.JTable();

        setBackground(new java.awt.Color(254, 234, 176));
        setClosable(true);
        setPreferredSize(new java.awt.Dimension(960, 540));

        MakeReservationMainPanal.setBackground(new java.awt.Color(254, 234, 176));

        MakeReservationPanal.setBackground(new java.awt.Color(254, 234, 176));
        MakeReservationPanal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Make Reservation", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 18))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel1.setText("Room Number");

        jLabel2.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel2.setText("Customer Name");

        txtMobileNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMobileNumberActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel3.setText("CNIC");

        jLabel4.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel4.setText("Address");

        txtAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddressActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel5.setText("Mobile Number");

        txtCnic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCnicActionPerformed(evt);
            }
        });

        btnReserveRoom.setBackground(java.awt.Color.white);
        btnReserveRoom.setText("Reserve Room");
        btnReserveRoom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReserveRoomActionPerformed(evt);
            }
        });

        cmbRoomNumber.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel6.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel6.setText("From Date");

        jLabel7.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel7.setText("To Date");

        dateChooserFrom.setDateFormatString("yyyy-MM-dd");

        dateChooserTo.setDateFormatString("yyyy-MM-dd");

        javax.swing.GroupLayout MakeReservationPanalLayout = new javax.swing.GroupLayout(MakeReservationPanal);
        MakeReservationPanal.setLayout(MakeReservationPanalLayout);
        MakeReservationPanalLayout.setHorizontalGroup(
            MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MakeReservationPanalLayout.createSequentialGroup()
                .addGroup(MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(MakeReservationPanalLayout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addGroup(MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(MakeReservationPanalLayout.createSequentialGroup()
                                .addGroup(MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(cmbRoomNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(txtCustomerName)))
                            .addComponent(jLabel4)
                            .addComponent(txtAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(MakeReservationPanalLayout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(97, 97, 97)
                                .addComponent(jLabel5))
                            .addGroup(MakeReservationPanalLayout.createSequentialGroup()
                                .addGroup(MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(dateChooserFrom, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                                    .addComponent(txtCnic))
                                .addGap(18, 18, 18)
                                .addGroup(MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(dateChooserTo, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtMobileNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(MakeReservationPanalLayout.createSequentialGroup()
                        .addGap(388, 388, 388)
                        .addComponent(btnReserveRoom)))
                .addContainerGap(52, Short.MAX_VALUE))
        );
        MakeReservationPanalLayout.setVerticalGroup(
            MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MakeReservationPanalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(MakeReservationPanalLayout.createSequentialGroup()
                        .addGap(94, 94, 94)
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(dateChooserFrom, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(MakeReservationPanalLayout.createSequentialGroup()
                        .addGroup(MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(MakeReservationPanalLayout.createSequentialGroup()
                                .addGroup(MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(jLabel5)))
                                .addGap(18, 18, 18)
                                .addGroup(MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cmbRoomNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtCnic, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtMobileNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(MakeReservationPanalLayout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(txtCustomerName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(MakeReservationPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(MakeReservationPanalLayout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)
                                .addComponent(txtAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(MakeReservationPanalLayout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(18, 18, 18)
                                .addComponent(dateChooserTo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(18, 18, 18)
                .addComponent(btnReserveRoom, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                .addContainerGap())
        );

        jtMakeReservation.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jtMakeReservation.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtMakeReservationMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jtMakeReservation);

        javax.swing.GroupLayout MakeReservationMainPanalLayout = new javax.swing.GroupLayout(MakeReservationMainPanal);
        MakeReservationMainPanal.setLayout(MakeReservationMainPanalLayout);
        MakeReservationMainPanalLayout.setHorizontalGroup(
            MakeReservationMainPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MakeReservationMainPanalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(MakeReservationMainPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(MakeReservationPanal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        MakeReservationMainPanalLayout.setVerticalGroup(
            MakeReservationMainPanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MakeReservationMainPanalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(MakeReservationPanal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MakeReservationMainPanal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(MakeReservationMainPanal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtMobileNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMobileNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMobileNumberActionPerformed

    private void txtAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddressActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAddressActionPerformed

    private void btnReserveRoomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReserveRoomActionPerformed
        String cnic = txtCnic.getText().trim();
        String customerName = txtCustomerName.getText().trim();
        String mobileNumber = txtMobileNumber.getText().trim();
        String address = txtAddress.getText().trim();
        String fromDate = ((JTextField) dateChooserFrom.getDateEditor().getUiComponent()).getText();
        String toDate = ((JTextField) dateChooserTo.getDateEditor().getUiComponent()).getText();

        roomBooking = new RoomBooking();
        roomBooking.setCnic(cnic);
        roomBooking.setCustomerName(customerName);
        roomBooking.setMobileNumber(mobileNumber);
        roomBooking.setAddress(address);
        roomBooking.setFromDate(fromDate);
        roomBooking.setToDate(toDate);

        int roomId = ((Rooms) cmbRoomNumber.getSelectedItem()).getRoomId();
        roomBooking.setRoomId(roomId + "");

        if (cnic.equals("") || customerName.equals("") || mobileNumber.equals("")
                || address.equals("") || fromDate.equals("") || toDate.equals("")) {
            JOptionPane.showMessageDialog(this, "Empty data can not be stored.");
        } else {
            roomBookingServices.makeReservation(roomBooking, user);
            vectorRoomBooking = commonService.getVectorRoomBooking();
            RoomBookingTableModel model = new RoomBookingTableModel(vectorRoomBooking);
            jtMakeReservation.setModel(model);
        }
    }//GEN-LAST:event_btnReserveRoomActionPerformed

    private void txtCnicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCnicActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCnicActionPerformed

    private void jtMakeReservationMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtMakeReservationMouseClicked
        int rowIndex = jtMakeReservation.getSelectedRow();
        int choice = -2;

        choice = JOptionPane.showConfirmDialog(this, "Are you sure you want to cancel this reservation.");

        int bookingId = 0;
        if (choice == 0) {
            bookingId = ((RoomBookingTableModel) jtMakeReservation.getModel()).getVectorRoomBooking().get(rowIndex).getId();
            roomBookingServices.cancelReservation(bookingId, user);
            vectorRoomBooking = commonService.getVectorRoomBooking();
            RoomBookingTableModel model = new RoomBookingTableModel(vectorRoomBooking);
            jtMakeReservation.setModel(model);
        }
    }//GEN-LAST:event_jtMakeReservationMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JPanel MakeReservationMainPanal;
    public javax.swing.JPanel MakeReservationPanal;
    private javax.swing.JButton btnReserveRoom;
    private javax.swing.JComboBox<String> cmbRoomNumber;
    private com.toedter.calendar.JDateChooser dateChooserFrom;
    private com.toedter.calendar.JDateChooser dateChooserTo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jtMakeReservation;
    private javax.swing.JTextField txtAddress;
    private javax.swing.JTextField txtCnic;
    private javax.swing.JTextField txtCustomerName;
    private javax.swing.JTextField txtMobileNumber;
    // End of variables declaration//GEN-END:variables
}
