/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Custom_Table;

import javax.swing.*;
import javax.swing.table.*;
public class CTest 
{
  public static void main(String args[])
  {
    JFrame jf=new JFrame("Table with cell spanning");

    CMap m=new CMap1();
    
    TableModel tm = new DefaultTableModel(7,25);
    
    jf.getContentPane().add(new JScrollPane(new CTable(m,tm)));
    jf.setDefaultCloseOperation(jf.EXIT_ON_CLOSE);
    jf.setSize(900, 900);
    //jf.show();
    
    String data = "Room No = FF-101";
    data = data + "\n Asad Khan \n vechicle = ICT-445";
    tm.setValueAt(data, 3, 3);
    
    jf.setVisible(true);
  }
}
