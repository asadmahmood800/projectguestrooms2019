/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import entity.FoodOrdersHistory;
import entity.RoomFoodOrder;
import entity.User;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author faraz
 */
public class RoomFoodOrderService {

    public int addroomFoodDetail(Vector<RoomFoodOrder> vectorRoomFoodOrder, User user) {
        int status = 0;
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String bulkId = System.currentTimeMillis() + "";
        int size = vectorRoomFoodOrder.size();

        int userId = user.getUserId();

        int i = 0;
        String query = "";

        int roomBookingId = 0;
        int foodItemId = 0;
        String quantity = "";
        String unitPrice = "";
        String totalPrice = "";

        try {

            for (i = 0; i < size; i++) {
                roomBookingId = vectorRoomFoodOrder.get(i).getRoomBooking().getId();
                foodItemId = vectorRoomFoodOrder.get(i).getFoodItem().getId();
                quantity = vectorRoomFoodOrder.get(i).getQuantity();
                unitPrice = vectorRoomFoodOrder.get(i).getFoodItem().getFoodItemPrice();
                totalPrice = vectorRoomFoodOrder.get(i).getTotalPrice();

                query = "INSERT INTO `room_food_order_details`(`room_booking_id`, `food_item_id`, `quantity`, `unit_price`, `total_price`, `inserted_by_user_id`, `insertion_date_time`, `bulk_id`)"
                        + " VALUES ("+roomBookingId+", "
                        +foodItemId + ", "
                        +quantity+", " + unitPrice+ ", " + totalPrice + "," + userId
                        + ", `getCurrentDateTime`(), "+ "'"
                        +bulkId+ "');";
                sql.executeUpdate(query);
                status++;
            }

            sql.commit();
            
            if (status >= 1) {
            JOptionPane.showMessageDialog(null, "Room Food Order Detail is Successfully added...!");
            } else {
                JOptionPane.showMessageDialog(null, "some problum has been occured try again letter...!");
            }
            
            generateBill(vectorRoomFoodOrder, user);
            
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return status;
    }
    
    public void generateBill(Vector<RoomFoodOrder> vectorRoomFoodOrder, User user) {
        
        try {
            
            Document d = new Document();
            String pdfNameDate = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            PdfWriter.getInstance(d, new FileOutputStream("src/reports/food_bill" + pdfNameDate + ".pdf"));
            d.open();
            
            Paragraph header = new Paragraph("Room Reservation", FontFactory.getFont(FontFactory.TIMES_BOLD, 20, Font.BOLD, BaseColor.BLUE));
            header.setAlignment(Element.ALIGN_CENTER);
            d.add(header);
            
            Image image = Image.getInstance(getClass().getResource("/images/Logo.png"));

            image.scaleAbsolute(100, 100);
            image.setAbsolutePosition(30, 730);
            Paragraph p = new Paragraph();
            p.setAlignment(Element.ALIGN_CENTER);
            p.add(image);
            d.add(p);

            Paragraph phone = new Paragraph("Phone# : +92316-1217680");
            phone.setAlignment(Element.ALIGN_CENTER);
            d.add(phone);

            Paragraph date = new Paragraph("Date : " + new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(Calendar.getInstance().getTime()));
            date.setAlignment(Element.ALIGN_CENTER);
            d.add(date);
            
            Paragraph space = new Paragraph("         ");
            space.setAlignment(Element.ALIGN_CENTER);
            d.add(space);
            
            
            
            PdfPTable t = new PdfPTable(3);
            Font fontH1 = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
            
            PdfPCell cell = new PdfPCell(new Paragraph("Room # " + vectorRoomFoodOrder.get(0).getRoomBooking().getRoomId() , FontFactory.getFont(FontFactory.TIMES_BOLD, 14, Font.BOLD,BaseColor.WHITE)));
            cell.setColspan(4);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.BLUE);
            t.addCell(cell);
            
            t.addCell(new PdfPCell(new Phrase("S.N.O", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Item Name", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Quantity", fontH1)));
            t.setWidths(new int[]{14, 30,30});
            
            
           
            for (int j=1,i = 0; i < vectorRoomFoodOrder.size() ; i++,j++) {
                  t.addCell(j + "");
                  t.addCell(vectorRoomFoodOrder.get(i).getFoodItem().getFoodItemName());
                  t.addCell(vectorRoomFoodOrder.get(i).getQuantity());
                }
            
            d.add(t);

            d.close();
            
            File pdfFile = new File("src/reports/food_bill"+ pdfNameDate + ".pdf");
            if (pdfFile.exists()) {
                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(pdfFile);
                    System.out.println("File Has Been Opened Successfully!");
                } else {
                    System.out.println("Desktop Not Supported!");
                }
            } else {
                System.out.println("PDF File Is Not Generated");
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }

}
    
    
    public void generateFoodOrderHistoryBill(Vector<FoodOrdersHistory> vectorFoodOrderHistory) {
        
        try {
            
            Document d = new Document();
            String pdfNameDate = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            PdfWriter.getInstance(d, new FileOutputStream("src/reports/food_bill" + pdfNameDate + ".pdf"));
            d.open();
            
            Paragraph header = new Paragraph("Room Reservation", FontFactory.getFont(FontFactory.TIMES_BOLD, 20, Font.BOLD, BaseColor.BLUE));
            header.setAlignment(Element.ALIGN_CENTER);
            d.add(header);
            
            Image image = Image.getInstance(getClass().getResource("/images/Logo.png"));

            image.scaleAbsolute(100, 100);
            image.setAbsolutePosition(30, 730);
            Paragraph p = new Paragraph();
            p.setAlignment(Element.ALIGN_CENTER);
            p.add(image);
            d.add(p);

            Paragraph phone = new Paragraph("Phone# : +92316-1217680");
            phone.setAlignment(Element.ALIGN_CENTER);
            d.add(phone);

            Paragraph date = new Paragraph("Date : " + new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(Calendar.getInstance().getTime()));
            date.setAlignment(Element.ALIGN_CENTER);
            d.add(date);
            
            Paragraph space = new Paragraph("         ");
            space.setAlignment(Element.ALIGN_CENTER);
            d.add(space);
            
            Paragraph space1 = new Paragraph("         ");
            space1.setAlignment(Element.ALIGN_CENTER);
            d.add(space1);
            
            Paragraph space2 = new Paragraph("         ");
            space2.setAlignment(Element.ALIGN_CENTER);
            d.add(space2);
            
            
            PdfPTable t = new PdfPTable(8);
            Font fontH1 = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
            
            PdfPCell cell = new PdfPCell(new Paragraph("Room No " + vectorFoodOrderHistory.get(0).getRoomNumber() , FontFactory.getFont(FontFactory.TIMES_BOLD, 14, Font.BOLD,BaseColor.WHITE)));
            cell.setColspan(9);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.BLUE);
            t.addCell(cell);
            
            t.addCell(new PdfPCell(new Phrase("S/No", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Customer", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Mobile No", fontH1)));
            t.addCell(new PdfPCell(new Phrase("CNIC", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Food Item", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Quantity", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Unit Price", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Total Price", fontH1)));
            t.setWidths(new int[]{4,9,9,9,7,7,7,7});
            
            
           
            for (int j=1,i = 0; i < vectorFoodOrderHistory.size() ; i++,j++) {
                  t.addCell(j + "");
                  t.addCell(vectorFoodOrderHistory.get(i).getCustomerName());
                  t.addCell(vectorFoodOrderHistory.get(i).getMobileNumber());
                  t.addCell(vectorFoodOrderHistory.get(i).getCnic());
                  t.addCell(vectorFoodOrderHistory.get(i).getFoodItemId());
                  t.addCell(vectorFoodOrderHistory.get(i).getQuantity());
                  t.addCell(vectorFoodOrderHistory.get(i).getUnitPrice());
                  t.addCell(vectorFoodOrderHistory.get(i).getToatalPrice());
                }
            
            d.add(t);

            d.close();
            
            File pdfFile = new File("src/reports/food_bill"+ pdfNameDate + ".pdf");
            if (pdfFile.exists()) {
                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(pdfFile);
                    System.out.println("File Has Been Opened Successfully!");
                } else {
                    System.out.println("Desktop Not Supported!");
                }
            } else {
                System.out.println("PDF File Is Not Generated");
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        
    }

}

