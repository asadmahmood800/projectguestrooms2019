/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import entity.RoomBooking;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author faraz
 */
public class CloseBillService {

    public void saveBillDetails(int roomBookingId, String paymentMethodId, RoomBooking roomBooking) { //final closing
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        if (paymentMethodId.equals("CHEQUE") && (roomBooking.getChequeNumber().equals("")
                || roomBooking.getBankName().equals("") || roomBooking.getBranchName().equals(""))) {
            JOptionPane.showMessageDialog(null, "Please provide cheque details.");
        } else {
            String query = "UPDATE `room_booking` SET `payment_method` = '"
                    + paymentMethodId + "' WHERE `id` = '"
                    + roomBookingId + "';";

            String queryBank = "UPDATE `room_booking` "
                    + "SET `cheque_number`= '"
                    + roomBooking.getChequeNumber() + "' ,`bank_name`= '"
                    + roomBooking.getBankName() + "',`branch_name`='"
                    + roomBooking.getBranchName() + "' WHERE `id` = '"
                    + roomBookingId + "';";

            String query1 = "INSERT INTO `room_bills`(`room_booking_id`, `title`, `rate`, `quantity`, `amount`, `insertion_date_time`, `inserted_by_user_id`) \n"
                    + "VALUES ("
                    + roomBookingId + ", 'Room Rent', "
                    + (roomBooking.getTotalRoomRent() / roomBooking.getNumberOfDays())
                    +"," + roomBooking.getNumberOfDays() + "," +  roomBooking.getTotalRoomRent() + ", `getCurrentDateTime`(), 1);";

            String query2 = "INSERT INTO `room_bills`(`room_booking_id`, `title`, `rate`, `quantity`, `amount`, `insertion_date_time`, `inserted_by_user_id`) \n"
                    + "VALUES ("
                    + roomBookingId + ", 'Food', 0, 0, "
                    + roomBooking.getTotalFoodAmount()
                    + ", `getCurrentDateTime`(), 1);";

            try {

                sql.executeUpdate(query);

                if (paymentMethodId.equals("CHEQUE")) {
                    sql.executeUpdate(queryBank);
                }

                /**
                 * here i will insert the data in room bill details table
                 */
                sql.executeUpdate(query1);
                sql.executeUpdate(query2);
                /**
                 * room bills coding ends here
                 */
                sql.commit();

                printGeneratBill(roomBooking, paymentMethodId);
                JOptionPane.showMessageDialog(null, "Bill is closed Successfully...!");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                sql.disconnect();
            }
        }

    }
    
    public void printGeneratBill(RoomBooking roomBooking, String paymentMethodId) {
        
        paymentMethodId = paymentMethodId.replaceAll("_", " ");
        

        try {

            Document d = new Document();
            String pdfNameDate = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            PdfWriter.getInstance(d, new FileOutputStream("src/reports/room_bill" + pdfNameDate + ".pdf"));
            d.open();

            Paragraph header = new Paragraph("Room Reservation", FontFactory.getFont(FontFactory.TIMES_BOLD, 20, Font.BOLD, BaseColor.BLUE));
            header.setAlignment(Element.ALIGN_CENTER);
            d.add(header);

            Image image = Image.getInstance(getClass().getResource("/images/Logo.png"));

            image.scaleAbsolute(100, 100);
            image.setAbsolutePosition(30, 730);
            Paragraph p = new Paragraph();
            p.setAlignment(Element.ALIGN_CENTER);
            p.add(image);
            d.add(p);

            Paragraph phone = new Paragraph("Phone# : +92316-1217680");
            phone.setAlignment(Element.ALIGN_CENTER);
            d.add(phone);

            Paragraph date = new Paragraph("Date : " + new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(Calendar.getInstance().getTime()));
            date.setAlignment(Element.ALIGN_CENTER);
            d.add(date);

            Paragraph roomAndGuest = new Paragraph("Room No : " + roomBooking.getRoomNumber() + "  " + "Guest Name : " + roomBooking.getCustomerName());
            roomAndGuest.setAlignment(Element.ALIGN_CENTER);
            d.add(roomAndGuest);

            Paragraph fromToDate = new Paragraph("From " + roomBooking.getFromDate() + " To  " + roomBooking.getToDate());
            fromToDate.setAlignment(Element.ALIGN_CENTER);
            d.add(fromToDate);

            Paragraph numberOfDays = new Paragraph("No Of Days : " + roomBooking.getNumberOfDays());
            numberOfDays.setAlignment(Element.ALIGN_CENTER);
            d.add(numberOfDays);
            
            
            Paragraph payment = new Paragraph("Payment Method : " + paymentMethodId);
            payment.setAlignment(Element.ALIGN_CENTER);
            d.add(payment);
            
            Paragraph space = new Paragraph("         ");
            space.setAlignment(Element.ALIGN_CENTER);
            d.add(space);

            Paragraph space1 = new Paragraph("         ");
            space1.setAlignment(Element.ALIGN_CENTER);
            d.add(space1);

            PdfPTable t = new PdfPTable(5);
            Font fontH1 = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);

            
            PdfPCell cell = new PdfPCell(new Paragraph("Room # " + roomBooking.getRoomNumber(), FontFactory.getFont(FontFactory.TIMES_BOLD, 14, Font.BOLD, BaseColor.WHITE)));
            
            cell.setColspan(6);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.BLUE);
            t.addCell(cell);

            t.addCell(new PdfPCell(new Phrase("S.N.O", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Title", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Rate", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Quantity", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Total", fontH1)));
            t.setWidths(new int[]{7, 7, 7, 7, 7});

            t.addCell(new PdfPCell(new Phrase("1", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Room Rent", fontH1)));
            t.addCell(new PdfPCell(new Phrase(roomBooking.getTotalRoomRent() / roomBooking.getNumberOfDays() + "", fontH1)));
            t.addCell(new PdfPCell(new Phrase(roomBooking.getNumberOfDays() + "", fontH1)));
            t.addCell(new PdfPCell(new Phrase(roomBooking.getTotalRoomRent() + "", fontH1)));

            t.addCell(new PdfPCell(new Phrase("2", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Food", fontH1)));
            t.addCell(new PdfPCell(new Phrase("0", fontH1)));
            t.addCell(new PdfPCell(new Phrase("0", fontH1)));
            t.addCell(new PdfPCell(new Phrase(roomBooking.getTotalFoodAmount() + "", fontH1)));
            
            t.addCell(new PdfPCell(new Phrase("", fontH1)));
            t.addCell(new PdfPCell(new Phrase("", fontH1)));
            t.addCell(new PdfPCell(new Phrase("", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Grand Total", fontH1)));
            t.addCell(new PdfPCell(new Phrase(roomBooking.getGrandTotal()+ "", fontH1)));

            d.add(t);

            d.close();

            File pdfFile = new File("src/reports/room_bill" + pdfNameDate + ".pdf");
            if (pdfFile.exists()) {
                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(pdfFile);
                    System.out.println("File Has Been Opened Successfully!");
                } else {
                    System.out.println("Desktop Not Supported!");
                }
            } else {
                System.out.println("PDF File Is Not Generated");
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

}
