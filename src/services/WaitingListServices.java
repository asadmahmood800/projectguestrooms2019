/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entity.User;
import entity.WaitingList;
import java.sql.SQLException;
import util.SQLQueryUtil;

/**
 *
 * @author ASAD MAHMOOD
 */
public class WaitingListServices {
    
    public void waitingListSave(WaitingList waitingList, User user) {
        
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        
        String query = "INSERT INTO `waiting_list` (`customer_name`, `cnic_number`, `address`, `phone_number`, `from_date`, `to_date`, `inserted_by_user_id`, `insertion_date_time`, `is_cancelled`, `is_room_allotted`) "
                + "VALUES ('"
                +waitingList.getCustomerName()+ "', '"
                +waitingList.getCnic()+ "', 'Unknown Address', '"
                +waitingList.getPhoneNumber()+ "','"
                +waitingList.getFromDate() +"' ,'"
                +waitingList.getToDate() +"', '"
                +user.getUserId() +"', `getCurrentDate`(), '0', '0');";
            
        try {
            sql.executeUpdate(query);
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally{
            sql.disconnect();
        }
    }
}
