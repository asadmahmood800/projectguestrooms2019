/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;


import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import entity.FoodItems;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;

import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author faraz
 */
public class FoodItemService {

    public int addFoodItems(FoodItems foodItems) {

        int status = 0;

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet = null;

        String queryCheck = "SELECT COUNT(*) AS `count` FROM `food_items` "
                + "WHERE `item_name` LIKE('"
                + foodItems.getFoodItemName() + "');";

        String query = "INSERT INTO `food_items`(`item_name`, `price`, "
                + "`inserted_by_user_id`, `insertion_date_time`) "
                + "VALUES ('"
                + foodItems.getFoodItemName() + "','"
                + foodItems.getFoodItemPrice() + "','2',`getCurrentDateTime`());";

        try {
            resultSet = sql.executeQuery(queryCheck);
            resultSet.next();

            if (resultSet.getInt("count") == 0) {
                status = sql.executeUpdate(query);
                sql.commit();
            } else {
                status = 2;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return status;
    }

    public void updateFoodItem(FoodItems foodItems) {

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String queryCheck = "SELECT COUNT(*) AS `count` FROM `food_items` "
                + "WHERE `item_name` LIKE('"
                + foodItems.getFoodItemName() + "') AND `id` != '"
                + foodItems.getId() + "';";

        String queryUpdate = "UPDATE `food_items` SET `item_name`='"
                + foodItems.getFoodItemName() + "',`price`='"
                + foodItems.getFoodItemPrice() + "',`inserted_by_user_id`='1',`insertion_date_time`=`getCurrentDateTime`()"
                + " WHERE `id` = '"
                + foodItems.getId() + "';";

        try {
            ResultSet resutlSet;
            resutlSet = sql.executeQuery(queryCheck);
            resutlSet.next();
            int count = resutlSet.getInt("count");

            if (count == 0) {
                sql.executeUpdate(queryUpdate);
                sql.commit();

                JOptionPane.showMessageDialog(null, "Record Updated successfully...!");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * the below method will print all food items in a pdf file. to generate pdf
     * we will use itext report library. the jar file is included in lib folder
     * @param vectorFoodItemPrint
     */
    
    public void printAllFoodItems(Vector<FoodItems> vectorFoodItemPrint) {
        try {
            Document d = new Document();
            String pdfNameDate = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            PdfWriter.getInstance(d, new FileOutputStream("src/reports/food_print" + pdfNameDate + ".pdf"));
            d.open();
            
            Paragraph header = new Paragraph("Room Reservation", FontFactory.getFont(FontFactory.TIMES_BOLD, 20, Font.BOLD, BaseColor.BLUE));
            header.setAlignment(Element.ALIGN_CENTER);
            d.add(header);
            
            Image image = Image.getInstance(getClass().getResource("/images/Logo.png"));

            image.scaleAbsolute(100, 100);
            image.setAbsolutePosition(30, 730);
            Paragraph p = new Paragraph();
            p.setAlignment(Element.ALIGN_CENTER);
            p.add(image);
            d.add(p);

            Paragraph phone = new Paragraph("Phone# : +92316-1217680");
            phone.setAlignment(Element.ALIGN_CENTER);
            d.add(phone);

            Paragraph date = new Paragraph("Date : " + new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(Calendar.getInstance().getTime()));
            date.setAlignment(Element.ALIGN_CENTER);
            d.add(date);
            
            Paragraph space2 = new Paragraph("         ");
            space2.setAlignment(Element.ALIGN_CENTER);
            d.add(space2);
            
            
            PdfPTable t = new PdfPTable(3);
            Font fontH1 = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
            
            PdfPCell cell = new PdfPCell(new Paragraph("Order Bill", FontFactory.getFont(FontFactory.TIMES_BOLD, 14, Font.BOLD,BaseColor.WHITE)));
            cell.setColspan(4);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.BLUE);
            t.addCell(cell);
            
            t.addCell(new PdfPCell(new Phrase("S.N.O", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Item Name", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Price", fontH1)));
            t.setWidths(new int[]{20,20,20});
            
          
           
            for (int j=1,i = 0; i < vectorFoodItemPrint.size() ; i++,j++) {
                   t.addCell(j + "");
                   t.addCell(vectorFoodItemPrint.get(i).getFoodItemName());
                   t.addCell(vectorFoodItemPrint.get(i).getFoodItemPrice());
                }
            
            d.add(t);

            d.close();
            
            File pdfFile = new File("src/reports/food_print"+ pdfNameDate + ".pdf");
            if (pdfFile.exists()) {
                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(pdfFile);
                    System.out.println("File Has Been Opened Successfully!");
                } else {
                    System.out.println("Desktop Not Supported!");
                }
            } else {
                System.out.println("PDF File Is Not Generated");
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

}
