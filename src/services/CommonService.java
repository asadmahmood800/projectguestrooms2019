/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entity.FoodItems;
import entity.FoodOrdersHistory;
import entity.PaymentMethod;
import entity.RoomBills;
import entity.RoomBooking;
import entity.RoomFoodOrder;
import entity.RoomType;
import entity.Rooms;
import entity.SearchingCriteria;
import entity.User;
import entity.WaitingList;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author faraz
 */

/*
ALTER TABLE `room_booking` ADD COLUMN
`cancelled_by_user_id` INT(11) NOT NULL DEFAULT 0
AFTER `branch_name`;


ALTER TABLE `room_booking` ADD COLUMN
`cancellation_date_time` DATETIME  NULL DEFAULT NULL
AFTER `cancelled_by_user_id`;

CREATE TABLE IF NOT EXISTS `payment_methods` (
    `id` VARCHAR(20) NOT NULL UNIQUE,
    `type` VARCHAR(20) NOT NULL UNIQUE
    )ENGINE=INNODB DEFAULT charset utf8;


INSERT INTO `payment_methods`(`id`, `type`) 
VALUES ('CASH', 'Cash'),
('CHEQUE', 'Cheque'),
('CREDIT_CARD', 'Credit Card'),
('PENDING', 'Pending');


ALTER TABLE `room_booking` ADD CONSTRAINT
`fk_room_booking_payment_methods` 
FOREIGN KEY(`payment_method`) REFERENCES
`payment_methods`(`id`);

 */
public class CommonService {

    Vector<Rooms> vectorRooms;

    public Vector<User> getVectorUsers() {
        Vector<User> vectorUsers = new Vector<>();

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String query = "SELECT * FROM `users` ORDER BY `id` ASC;";

        ResultSet resultSet;
        User user;

        try {
            resultSet = sql.executeQuery(query);

            while (resultSet.next()) {
                user = new User();
                user.setUserId(resultSet.getInt("id"));
                user.setDisplayName(resultSet.getString("display_name"));
                user.setUserName(resultSet.getString("user_name"));
                user.setIsActive(resultSet.getInt("is_active"));

                vectorUsers.add(user);

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return vectorUsers;
    }

    public Vector<FoodItems> getVectorFoodItems() {

        Vector<FoodItems> vectorfoodItems = new Vector<>();

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String query = "SELECT * FROM `food_items` ORDER BY `id` ASC;";

        ResultSet resultSet;
        FoodItems foodItems;

        try {

            resultSet = sql.executeQuery(query);

            while (resultSet.next()) {
                foodItems = new FoodItems();

                foodItems.setId(resultSet.getInt("id"));
                foodItems.setFoodItemName(resultSet.getString("item_name"));
                foodItems.setFoodItemPrice(resultSet.getString("price"));

                vectorfoodItems.add(foodItems);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return vectorfoodItems;
    }

    public Vector<RoomType> getVectorRoomTypes() {
        
        Vector<RoomType> vectorRoomTypes = new Vector<>();

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        String query = "SELECT * FROM `rooms_types` ORDER BY `id` ASC;";
        ResultSet resultSet;
        RoomType roomType;

        try {
            resultSet = sql.executeQuery(query);
            while (resultSet.next()) {
                roomType = new RoomType(resultSet.getInt("id"), resultSet.getString("types"));
                vectorRoomTypes.add(roomType);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return vectorRoomTypes;
    }

    public Vector<Rooms> getVectorRooms() {

        vectorRooms = new Vector<>();

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        String query = "SELECT `r`.*, `rt`.`id` AS `rt_id`,`rt`.`types` AS `rt_label` , `is_out_of_order` , `per_day_charges` FROM `rooms` AS `r`,\n"
                + "`rooms_types` AS `rt` WHERE\n"
                + "`r`.`room_types_id` = `rt`.`id` \n"
                + "ORDER BY `r`.`room_number` ASC;";
        ResultSet resultSet;
        Rooms room;

        try {
            resultSet = sql.executeQuery(query);
            while (resultSet.next()) {
                room = new Rooms();
                room.setRoomId(resultSet.getInt("id"));
                room.setRoomNumber(resultSet.getString("room_number"));
                room.setRoomTypeLabel(resultSet.getString("rt_label"));
                room.setRoomTypeId(resultSet.getInt("rt_id"));
                
                room.setPerDayCharges(resultSet.getInt("per_day_charges"));
                room.setIsOutOfOrder(resultSet.getInt("is_out_of_order"));
                vectorRooms.add(room);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vectorRooms;
    }

    public Vector<Rooms> getVectorRoomNumber() {
        vectorRooms = new Vector<>();
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        String query = "SELECT * FROM `rooms` ORDER BY `id` ASC;";
        ResultSet resultSet;
        Rooms room;

        try {
            resultSet = sql.executeQuery(query);
            while (resultSet.next()) {
                room = new Rooms();
                room.setRoomId(resultSet.getInt("id"));
                room.setRoomNumber(resultSet.getString("room_number"));
                vectorRooms.add(room);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vectorRooms;
    }

    public Vector<RoomBooking> getVectorRoomBooking() {
        Vector<RoomBooking> vectorRoomBooking = new Vector<>();
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        //String bulkId = System.currentTimeMillis() + "";
        
        String query = "SELECT `r`.`room_number`, `rb`.`id`, `rb`.`customer_name`, \n"
                + "`rb`.`mobile_number`, DATE_FORMAT(`rb`.`from_date`, '%d %b %Y') AS `from_date`, DATE_FORMAT(`rb`.`to_date`, '%d %b %Y') AS `to_date`, \n"
                + "`rb`.`cnic` FROM `rooms` AS `r`, `room_booking` AS `rb`\n"
                + "WHERE \n"
                + " /*(\n"
                + "    `getCurrentDate`() BETWEEN `from_date` AND \n"
                + " `to_date` \n"
                + "    ) AND */  `r`.`id` = `rb`.`room_id` AND `is_cancelled` = 0 "
                + "AND `is_left` = 0 \n"
                + " ORDER BY `rb`.`from_date` ASC;";
        System.out.println(query);
        ResultSet resultSet;
        RoomBooking roomBooking;

        try {
            resultSet = sql.executeQuery(query);
            while (resultSet.next()) {
                roomBooking = new RoomBooking();
                roomBooking.setRoomId(resultSet.getString("room_number"));
                roomBooking.setId(resultSet.getInt("id")); //this is roomBooking id
                roomBooking.setCustomerName(resultSet.getString("customer_name"));
                roomBooking.setCnic(resultSet.getString("cnic"));
                roomBooking.setMobileNumber(resultSet.getString("mobile_number"));
                roomBooking.setFromDate(resultSet.getString("from_date"));
                roomBooking.setToDate(resultSet.getString("to_date"));
                vectorRoomBooking.add(roomBooking);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return vectorRoomBooking;
    }

    public Vector<WaitingList> getVectorWaitingList() {
        Vector<WaitingList> vectorWaitingList = new Vector<>();
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        String query = "SELECT `customer_name`,`cnic_number`,`phone_number`,\n"
                + "DATE_FORMAT(`from_date`, '%d %b %Y') AS `from_date`,\n"
                + "DATE_FORMAT(`to_date`, '%d %b %Y') AS `to_date` \n"
                + "FROM `waiting_list`\n"
                + "ORDER BY `from_date` ASC;";
        ResultSet resultSet;
        WaitingList waitingList;

        try {
            resultSet = sql.executeQuery(query);
            while (resultSet.next()) {
                waitingList = new WaitingList();
                waitingList.setCustomerName(resultSet.getString("customer_name"));
                waitingList.setCnic(resultSet.getString("cnic_number"));
                waitingList.setPhoneNumber(resultSet.getString("phone_number"));
                waitingList.setFromDate(resultSet.getString("from_date"));
                waitingList.setToDate(resultSet.getString("to_date"));
                vectorWaitingList.add(waitingList);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return vectorWaitingList;
    }

    public Vector<RoomFoodOrder> getVectorRoomFoodOrder(RoomFoodOrder roomFoodOrder) {
        Vector<RoomFoodOrder> vectorRoomFoodOrder = new Vector<>();
        roomFoodOrder = new RoomFoodOrder();
//        SQLQueryUtil sql = new SQLQueryUtil();
//        sql.connect(false);
//        String query = "SELECT * FROM `room_food_order_details` ORDER BY `id` ASC;";
//        ResultSet resultSet;
//        RoomFoodOrder roomFoodOrder;
//        RoomBooking roomBooking;
//        try {
//            resultSet = sql.executeQuery(query);
//            while (resultSet.next()) {
//                roomFoodOrder = new RoomFoodOrder();
//                //roomFoodOrder.setRoomBooking(resultSet.getString("room_booking_id"));
//
//            }
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//        }

        String bulkId = System.currentTimeMillis() + "";
        roomFoodOrder.getFoodItem();
        roomFoodOrder.getQuantity();
        roomFoodOrder.getTotalPrice();
        roomFoodOrder.getRoomFoodOrderId();
        roomFoodOrder.setBulkId(bulkId);
        vectorRoomFoodOrder.add(roomFoodOrder);
          
        return vectorRoomFoodOrder;
        
    }

    public Vector<SearchingCriteria> getSearchingCriteriaVector() {
        Vector<SearchingCriteria> criteriaVector = new Vector<>();
        SearchingCriteria criteria;

        criteria = new SearchingCriteria("CUSTOMER_NAME", "Customer Name");
        criteriaVector.add(criteria);

        criteria = new SearchingCriteria("CNIC", "CNIC");
        criteriaVector.add(criteria);

        criteria = new SearchingCriteria("MOBILE_NUMBER", "Mobile Number");
        criteriaVector.add(criteria);

        return criteriaVector;
    }

    public Vector<RoomBooking> getGuestHistory(String id, String data) {
        
        Vector<RoomBooking> vectorRoomBooking = new Vector<>();
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        RoomBooking roomBooking = null;
        
        String query = "";
        if (id.equals("CNIC")) {
            query = "SELECT `r`.`room_number`, `r`.`per_day_charges`, `rb`.*,  (DATEDIFF(`rb`.`to_date`, `rb`.`from_date`) + 1) AS `number_of_days` FROM `rooms` AS `r`, `room_booking` AS `rb` \n" +
"WHERE `r`.`id` = `rb`.`room_id` AND \n" +
"`rb`.`cnic`='" + data + "' ORDER BY `rb`.`from_date` DESC;";
            try {
                resultSet = sql.executeQuery(query);
                while (resultSet.next()) { 
                    roomBooking = new RoomBooking();
                    roomBooking.setRoomNumber(resultSet.getString("room_number"));
                    roomBooking.setFromDate(resultSet.getString("from_date"));
                    roomBooking.setToDate(resultSet.getString("to_date"));
                    roomBooking.setNumberOfDays(resultSet.getInt("number_of_days"));
                    roomBooking.setCustomerName(resultSet.getString("customer_name"));
                    roomBooking.setCnic(resultSet.getString("cnic"));
                    roomBooking.setMobileNumber(resultSet.getString("mobile_number"));
                    roomBooking.setAddress(resultSet.getString("address"));
                    
                    vectorRoomBooking.add(roomBooking);
                    
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            
        } else if (id.equals("CUSTOMER_NAME")) {
            query = "SELECT `r`.`room_number`,`r`.`per_day_charges`,  `rb`.*,  (DATEDIFF(`rb`.`to_date`, `rb`.`from_date`) + 1) AS `number_of_days` FROM `rooms` AS `r`, `room_booking` AS `rb` \n" +
"WHERE `r`.`id` = `rb`.`room_id` AND \n" +
"`rb`.`customer_name`='" + data + "' ORDER BY `rb`.`from_date` DESC;";
            
            try {
                resultSet = sql.executeQuery(query);
                while (resultSet.next()) {                    
                    roomBooking = new RoomBooking();
                    roomBooking.setRoomNumber(resultSet.getString("room_number"));
                    roomBooking.setFromDate(resultSet.getString("from_date"));
                    roomBooking.setToDate(resultSet.getString("to_date"));
                    roomBooking.setNumberOfDays(resultSet.getInt("number_of_days"));
                    roomBooking.setCustomerName(resultSet.getString("customer_name"));
                    roomBooking.setCnic(resultSet.getString("cnic"));
                    roomBooking.setMobileNumber(resultSet.getString("mobile_number"));
                    roomBooking.setAddress(resultSet.getString("address"));
                    
                    vectorRoomBooking.add(roomBooking);
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            
        } else if (id.equals("MOBILE_NUMBER")) {
            query = "SELECT `r`.`room_number`, `r`.`per_day_charges`, `rb`.*,  (DATEDIFF(`rb`.`to_date`, `rb`.`from_date`) + 1) AS `number_of_days` FROM `rooms` AS `r`, `room_booking` AS `rb` \n" +
"WHERE `r`.`id` = `rb`.`room_id` AND \n" +
"`rb`.`mobile_number`='" + data + "' ORDER BY `rb`.`from_date` DESC;";
            
            try {
                resultSet = sql.executeQuery(query);
                while (resultSet.next()) {                    
                    roomBooking = new RoomBooking();
                    roomBooking.setRoomNumber(resultSet.getString("room_number"));
                    roomBooking.setFromDate(resultSet.getString("from_date"));
                    roomBooking.setToDate(resultSet.getString("to_date"));
                    roomBooking.setNumberOfDays(resultSet.getInt("number_of_days"));
                    roomBooking.setCustomerName(resultSet.getString("customer_name"));
                    roomBooking.setCnic(resultSet.getString("cnic"));
                    roomBooking.setMobileNumber(resultSet.getString("mobile_number"));
                    roomBooking.setAddress(resultSet.getString("address"));
                    
                    vectorRoomBooking.add(roomBooking);
                }
            } catch (SQLException ex) { 
                ex.printStackTrace();
            }
        }
        
        return vectorRoomBooking;
        
    }
    
    
public Vector<RoomBooking> getVectorRoomBookingCurrentDate() {
        Vector<RoomBooking> vectorRoomBooking = new Vector<>();
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
                
        String query = "SELECT `r`.`room_number`, `rb`.`id`, `rb`.`customer_name`, \n"
                + "`rb`.`mobile_number`, DATE_FORMAT(`rb`.`from_date`, '%d %b %Y') AS `from_date`, DATE_FORMAT(`rb`.`to_date`, '%d %b %Y') AS `to_date`, \n"
                + "`rb`.`cnic` FROM `rooms` AS `r`, `room_booking` AS `rb`\n"
                + "WHERE \n"
                + " (\n"
                + "    `getCurrentDate`() BETWEEN `from_date` AND \n"
                + " `to_date` \n"
                + "    ) AND   `r`.`id` = `rb`.`room_id` AND `is_cancelled` = 0 "
                + "AND `is_left` = 0 \n"
                + " ORDER BY `rb`.`from_date` ASC;";
        System.out.println(query);
        ResultSet resultSet;
        RoomBooking roomBooking;

        try {
            resultSet = sql.executeQuery(query);
            while (resultSet.next()) {
                roomBooking = new RoomBooking();
                roomBooking.setRoomId(resultSet.getString("room_number"));
                roomBooking.setId(resultSet.getInt("id")); //this is roomBooking id
                roomBooking.setCustomerName(resultSet.getString("customer_name"));
                roomBooking.setCnic(resultSet.getString("cnic"));
                roomBooking.setMobileNumber(resultSet.getString("mobile_number"));
                roomBooking.setFromDate(resultSet.getString("from_date"));
                roomBooking.setToDate(resultSet.getString("to_date"));
                vectorRoomBooking.add(roomBooking);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return vectorRoomBooking;
    }


public Vector<PaymentMethod> getPaymentDetail() {
    
        Vector<PaymentMethod> vectorPaymentMethod = new Vector<>();

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        
        String query = "SELECT * FROM `payment_methods` WHERE `id` != 'PENDING';";
        
        ResultSet resultSet = null;
        PaymentMethod paymentMethod;
        
        try {
            resultSet =  sql.executeQuery(query);
            
            while (resultSet.next()) {  
                
                paymentMethod = new PaymentMethod();
                
                paymentMethod.setId(resultSet.getString("id"));
                paymentMethod.setType(resultSet.getString("type"));
                vectorPaymentMethod.add(paymentMethod);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    
    
        return vectorPaymentMethod;
}

    public Vector<RoomBills> getroombillsDetail(int roomBookingId) {
            
            Vector<RoomBills> vectorRoomBills = new Vector<>();
        
            SQLQueryUtil sql = new SQLQueryUtil();
            sql.connect(false);
            
            String query = "SELECT `room_booking_id`, `title`, `rate`, `quantity`, `amount` "
                    + "FROM `room_bills` WHERE `room_booking_id` = '"
                    + roomBookingId + "';";
            ResultSet resultSet;
            RoomBills roomBills;
           
            
            try {
                resultSet = sql.executeQuery(query);
                while (resultSet.next()) {
                roomBills = new RoomBills();
                roomBills.setRoombookingId(resultSet.getInt("room_booking_id"));
                roomBills.setTitle(resultSet.getString("title"));
                roomBills.setRate(resultSet.getInt("rate"));
                roomBills.setQuantity(resultSet.getInt("quantity"));
                roomBills.setTotalAmount(resultSet.getInt("amount"));
                   
                vectorRoomBills.add(roomBills);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        return vectorRoomBills;
        
    }
    
    
    
    
    public Vector<FoodOrdersHistory> getFoodOrderDetails(String id, String data) {
        
        Vector<FoodOrdersHistory> vectorFoodOrderHistory = new Vector<>();
        
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        ResultSet resultSet;
        FoodOrdersHistory foodOrderHistory;
        String query = "";
        
        if (id.equals("CUSTOMER_NAME")) {
            
            query = "SELECT `r`.`room_number`,`rb`.`customer_name`,`rb`.`mobile_number`"
                    + ",`rb`.`cnic`,`fi`.`item_name`,`rfod`.`quantity`,`rfod`.`unit_price`"
                    + ",`rfod`.`total_price` FROM `room_food_order_details` AS `rfod` , "
                    + "`room_booking` AS `rb`, `food_items` AS `fi`, `rooms` AS `r`"
                    + " WHERE `r`.`id` = `rb`.`room_id` "
                    + "AND `rfod`.`room_booking_id` = `rb`.`id` "
                    + "AND `rfod`.`food_item_id` = `fi`.`id`"
                    + " AND `rb`.`customer_name` = '"
                    + data + "';";
           
            try {
                resultSet = sql.executeQuery(query);
                while (resultSet.next()) {
                    foodOrderHistory = new FoodOrdersHistory();
                    foodOrderHistory.setRoomNumber(resultSet.getString("room_number"));
                    foodOrderHistory.setCustomerName(resultSet.getString("customer_name"));
                    foodOrderHistory.setMobileNumber(resultSet.getString("mobile_number"));
                    foodOrderHistory.setCnic(resultSet.getString("cnic"));
                    foodOrderHistory.setFoodItemId(resultSet.getString("item_name"));
                    foodOrderHistory.setQuantity(resultSet.getString("quantity"));
                    foodOrderHistory.setUnitPrice(resultSet.getString("unit_price"));
                    foodOrderHistory.setToatalPrice(resultSet.getString("total_price"));
                    
                    vectorFoodOrderHistory.add(foodOrderHistory);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            
           
            
        } else if (id.equals("CNIC")) {
            
            query = "SELECT `r`.`room_number`,`rb`.`customer_name`,`rb`.`mobile_number`"
                    + ",`rb`.`cnic`,`fi`.`item_name`,`rfod`.`quantity`,`rfod`.`unit_price`"
                    + ",`rfod`.`total_price` FROM `room_food_order_details` AS `rfod` , "
                    + "`room_booking` AS `rb`, `food_items` AS `fi`, `rooms` AS `r`"
                    + " WHERE `r`.`id` = `rb`.`room_id` "
                    + "AND `rfod`.`room_booking_id` = `rb`.`id` "
                    + "AND `rfod`.`food_item_id` = `fi`.`id`"
                    + " AND `rb`.`customer_name` = '"
                    + data + "';";
            
             try {
                resultSet = sql.executeQuery(query);
                while (resultSet.next()) {
                    foodOrderHistory = new FoodOrdersHistory();
                    foodOrderHistory.setRoomNumber(resultSet.getString("room_number"));
                    foodOrderHistory.setCustomerName(resultSet.getString("customer_name"));
                    foodOrderHistory.setMobileNumber(resultSet.getString("mobile_number"));
                    foodOrderHistory.setCnic(resultSet.getString("cnic"));
                    foodOrderHistory.setFoodItemId(resultSet.getString("item_name"));
                    foodOrderHistory.setQuantity(resultSet.getString("quantity"));
                    foodOrderHistory.setUnitPrice(resultSet.getString("unit_price"));
                    foodOrderHistory.setToatalPrice(resultSet.getString("total_price"));
                    
                    vectorFoodOrderHistory.add(foodOrderHistory);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            
        } else if (id.equals("MOBILE_NUMBER")) {
            
            query = "SELECT `r`.`room_number`,`rb`.`customer_name`,`rb`.`mobile_number`"
                    + ",`rb`.`cnic`,`fi`.`item_name`,`rfod`.`quantity`,`rfod`.`unit_price`"
                    + ",`rfod`.`total_price` FROM `room_food_order_details` AS `rfod` , "
                    + "`room_booking` AS `rb`, `food_items` AS `fi`, `rooms` AS `r`"
                    + " WHERE `r`.`id` = `rb`.`room_id` "
                    + "AND `rfod`.`room_booking_id` = `rb`.`id` "
                    + "AND `rfod`.`food_item_id` = `fi`.`id`"
                    + " AND `rb`.`customer_name` = '"
                    + data + "';";
            
             try {
                resultSet = sql.executeQuery(query);
                while (resultSet.next()) {
                    foodOrderHistory = new FoodOrdersHistory();
                    foodOrderHistory.setRoomNumber(resultSet.getString("room_number"));
                    foodOrderHistory.setCustomerName(resultSet.getString("customer_name"));
                    foodOrderHistory.setMobileNumber(resultSet.getString("mobile_number"));
                    foodOrderHistory.setCnic(resultSet.getString("cnic"));
                    foodOrderHistory.setFoodItemId(resultSet.getString("item_name"));
                    foodOrderHistory.setQuantity(resultSet.getString("quantity"));
                    foodOrderHistory.setUnitPrice(resultSet.getString("unit_price"));
                    foodOrderHistory.setToatalPrice(resultSet.getString("total_price"));
                    
                    vectorFoodOrderHistory.add(foodOrderHistory);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            
        }
        
        
        return vectorFoodOrderHistory;
    }
    
}

