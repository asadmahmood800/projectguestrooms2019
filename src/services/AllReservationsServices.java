/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entity.CustomPopupMenu;
import entity.Rooms;
import java.awt.Choice;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JPanel;
import util.SQLQueryUtil;
import views.AllReservationsModified;
import views.MainForm;
import views.MyMouseListener;

/**
 *
 * @author faraz
 */
public class AllReservationsServices {

    private int columnCount;
    private MainForm mainForm;
    private AllReservationsModified allReservationModified;
    
    public AllReservationsServices(MainForm mainForm, AllReservationsModified allReservationModified) {
        this.mainForm = mainForm;
        this.allReservationModified = allReservationModified;
    }

    public void generateHeaderRow(String date, JPanel panelScroll) {
        int year;
        int month;
        int day;

        int isLeapYear = 0;
        int isFebruary = 0;

        String dateArray[];

        dateArray = date.split("-");
        year = Integer.parseInt(dateArray[0]);
        month = Integer.parseInt(dateArray[1]);
        day = Integer.parseInt(dateArray[2]);

        if (month == 2) {
            isFebruary = 1;
        }

        if ((year % 4) == 0) {
            isLeapYear = 1;
        }

        columnCount = 29;

        if (isLeapYear == 1 && isFebruary == 1) {
            columnCount = 30;
        } else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            columnCount = 32;
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
            columnCount = 31;
        }

        JLabel label;

        label = new JLabel(" Room# / Date");

        panelScroll.add(label);
        label.setBounds(10, 10, 99, 39);
        label.setOpaque(true);
        //rgb(34,139,34) dark green color
        Color color = new Color(34, 139, 34);

        label.setBackground(color);
        label.setForeground(Color.WHITE);
        label.setVisible(true);

        int i = 0;
        int x = 110;
        int y = 10;

        for (i = 1; i < columnCount; i++) {
            label = new JLabel(" " + i);
            panelScroll.add(label);

            label.setBounds(x, y, 100, 39);
            label.setOpaque(true);
            //rgb(34,139,34) dark green color
            color = new Color(34, 139, 34);

            label.setBackground(color);
            label.setForeground(Color.WHITE);
            label.setVisible(true);
            label.setHorizontalAlignment(JLabel.CENTER);

            x = x + 101;

        }
        panelScroll.setVisible(true);

    }

    public void showAllReservations(String date, JPanel panelScroll, int roomTypeId) {
        //this method will create the top header row of our designed grid.
        panelScroll.removeAll();
        panelScroll.repaint();
        generateHeaderRow(date, panelScroll);

        /**
         * the rest of the data, reservation information and details will be
         * shown by this method.
         */
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String queryRooms = "SELECT `id`, `room_number` FROM `rooms` WHERE `room_types_id` = " + roomTypeId + " ORDER BY `id` asc;";

        ResultSet resultRooms;
        ResultSet resultRoomReservations;

        int i = 0;
        int x = 10;
        int y = 50;

        JLabel label;

        //rgb(34,139,34) dark green color
        Color color = new Color(34, 139, 34);

        //rgb(34,139,34) FREE ROOMS BACKGROUND COLOR
        Color freeRoomsBg = new Color(34, 139, 34);

        //RESERVED ROOMS BACKGROUND COLOR
        Color reservedRoomsBg = new Color(255, 183, 77);

        //OCCUPIED ROOMS BACKGROUND COLOR
        Color occupiedRoomsBg = new Color(194, 6, 11);

        //BILL GENERATED BUT PAYMENT IS PENDING ROOMS BACKGROUND COLOR
        Color pendingPayment = new Color(197, 19, 203);
        
        //LEFT / BILL GENERATED ROOMS BACKGROUND COLOR
        Color leftRoomsBg = new Color(12, 89, 185);

        int roomId = 0;
        int roomBookingId = 0;

        int generatedCellsCount = 0;
        String queryRoomBookingDetails = "";

        Vector<Rooms> vectorRooms = new Vector<>();
        Rooms room;

        String yearMonth = date.substring(0, 7);

        int monthNumberChecking = Integer.parseInt(date.substring(5, 7));

        System.out.println(monthNumberChecking);

        int numberOfDays = 0;
        String roomNumber;
        String customerName;
        String paymentMethod;
        String vehicleNumber;
        String contactNumber;

        String arrivalDateTime = "";

        String html = "";

        int isLeft = 0;

        int startMonth = 0;
        int endMonth = 0;

        int start = 0;
        int end = 0;

        int isEmptyCellGenerated = 0;
        int emptyCellCounterBetweenReservations = 1;
        
        try {
            resultRooms = sql.executeQuery(queryRooms);

            //THE BELOW METHOD WILL DISPLAY ROOM NUMBERS IN VERTICAL FORMAT
            generateRoomNumbers(vectorRooms, x, y, resultRooms, panelScroll);

            y = 50;

            //i fininshed the rooms result set.
            //HERE I WILL BRING RESERVATIONS OF THE SELECTED ROOM UNDER LOOP
            for (int k = 0; k < vectorRooms.size(); k++) {
                emptyCellCounterBetweenReservations = 1;
                queryRoomBookingDetails = "SELECT `id`, `room_id`, DATE_FORMAT(`from_date`, '%d') AS `start`, DATE_FORMAT(`to_date`, '%d') AS `end`, \n"
                        + "DATE_FORMAT(`from_date`, '%c') AS `start_month`, DATE_FORMAT(`to_date`, '%c') AS `end_month`,`mobile_number`, \n"
                        + "`customer_name`, `payment_method`, IFNULL(`vehicle_number`, '') AS `vehicle_number`, (DATEDIFF(`to_date`, `from_date`) + 1) AS `number_of_days`, \n"
                        + " IFNULL(`arrival_date_time`, '') AS `arrival_date_time`, `departure_date_time`, `is_left`, `from_date` FROM `room_booking` \n"
                        + "WHERE `room_id` = '" + vectorRooms.get(k).getRoomId()
                        + "' AND ('" + yearMonth + "' BETWEEN DATE_FORMAT(`from_date`, '%Y-%m') AND \n"
                        + " DATE_FORMAT(`to_date`, '%Y-%m')) AND `is_cancelled` = 0 ORDER BY `from_date` ASC;";
                resultRoomReservations = sql.executeQuery(queryRoomBookingDetails);

                x = 110;

                generatedCellsCount = 0;

                while (resultRoomReservations.next()) {

                    //THIS IS THE CORE LOGIC OF SHOWING ROOMS RESERVATIONS.
                    System.out.println(resultRoomReservations.getInt("id"));
                    numberOfDays = resultRoomReservations.getInt("number_of_days");

                    customerName = resultRoomReservations.getString("customer_name");
                    paymentMethod = resultRoomReservations.getString("payment_method");
                    contactNumber = resultRoomReservations.getString("mobile_number");
                    vehicleNumber = resultRoomReservations.getString("vehicle_number");

                    startMonth = resultRoomReservations.getInt("start_month");
                    endMonth = resultRoomReservations.getInt("end_month");

                    start = resultRoomReservations.getInt("start"); //from which date the reservation starts
                    end = resultRoomReservations.getInt("end"); //at which date the reservation ends

                    isLeft = resultRoomReservations.getInt("is_left");
                    arrivalDateTime = resultRoomReservations.getString("arrival_date_time");

                    roomBookingId = resultRoomReservations.getInt("id");

                    if (startMonth == endMonth) {
                        for (; emptyCellCounterBetweenReservations < start; emptyCellCounterBetweenReservations++) {
                            produceEmptyLabel(panelScroll, x, y);
                            x = x + 101;
                            generatedCellsCount++;
                            isEmptyCellGenerated = 1;
                        }

                        generateReservationBox(customerName, contactNumber, vehicleNumber, panelScroll, x, y, numberOfDays, isLeft, arrivalDateTime, leftRoomsBg, reservedRoomsBg, occupiedRoomsBg, roomBookingId, paymentMethod, pendingPayment);
                        generatedCellsCount = generatedCellsCount + numberOfDays;
                        emptyCellCounterBetweenReservations = emptyCellCounterBetweenReservations + numberOfDays;

                    } else if (startMonth < endMonth && endMonth == monthNumberChecking) {
                        numberOfDays = end;
                        generateReservationBox(customerName, contactNumber, vehicleNumber, panelScroll, x, y, numberOfDays, isLeft, arrivalDateTime, leftRoomsBg, reservedRoomsBg, occupiedRoomsBg, roomBookingId, paymentMethod,pendingPayment);
                        generatedCellsCount = generatedCellsCount + end;

                        emptyCellCounterBetweenReservations = emptyCellCounterBetweenReservations + numberOfDays;

                    } else if (startMonth < endMonth && startMonth == monthNumberChecking) {
                        for (; emptyCellCounterBetweenReservations < start; emptyCellCounterBetweenReservations++) {
                            produceEmptyLabel(panelScroll, x, y);
                            x = x + 101;
                            generatedCellsCount++;
                            isEmptyCellGenerated = 1;
                        }
                        numberOfDays = columnCount - start;
                        generateReservationBox(customerName, contactNumber, vehicleNumber, panelScroll, x, y, numberOfDays, isLeft, arrivalDateTime, leftRoomsBg, reservedRoomsBg, occupiedRoomsBg, roomBookingId, paymentMethod, pendingPayment);
                        generatedCellsCount = generatedCellsCount + numberOfDays;

                    }

                    if (isEmptyCellGenerated == 1) {
                        //emptyCellCounterBetweenReservations--;
                        //generatedCellsCount--;
                    }

                    x = x + numberOfDays * 101;

                }

                //it will generate empty cells at the end or a row
                for (; generatedCellsCount < columnCount - 1; generatedCellsCount++) {
                    produceEmptyLabel(panelScroll, x, y);
                    x = x + 101;
                }

                if (generatedCellsCount == 0) { // it means that there is no reservation in the selected month for this room
                    for (int l = 1; l < columnCount; l++) {
                        produceEmptyLabel(panelScroll, x, y);
                        x = x + 101;
                    }
                    y = y + 100;
                } else {
                    y = y + 100;
                }

            }

            //END
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void produceEmptyLabel(JPanel panelScroll, int x, int y) {
        JLabel label;

        label = new JLabel("");

        panelScroll.add(label);
        label.setBounds(x, y, 99, 99);
        label.setOpaque(true);
        //rgb(34,139,34) dark green color
        Color color = new Color(34, 139, 34);

        label.setBackground(color);
        label.setVisible(true);

    }

    /**
     *
     * @param vectorRooms
     * @param x
     * @param y
     * @param resultRooms
     * @param panelScroll
     */
    private void generateRoomNumbers(Vector<Rooms> vectorRooms, int x, int y, ResultSet resultRooms, JPanel panelScroll) {
        Rooms room;
        JLabel label;
        int roomId = 0;
        Color color;

        try {
            while (resultRooms.next()) {
                label = new JLabel(resultRooms.getString("room_number"));
                roomId = resultRooms.getInt("id");
                panelScroll.add(label);
                label.setBounds(x, y, 99, 99);
                label.setOpaque(true);
                //rgb(34,139,34) dark green color
                color = new Color(34, 139, 34);

                label.setBackground(color);
                label.setForeground(Color.WHITE);
                label.setVisible(true);
                y = y + 100;

                room = new Rooms();
                room.setRoomId(roomId);
                room.setRoomNumber(resultRooms.getString("room_number"));

                vectorRooms.add(room);

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    private void generateReservationBox(String customerName, String contactNumber, String vehicleNumber,
            JPanel panelScroll, int x, int y, int numberOfDays, int isLeft,
            String arrivalDateTime, Color leftRoomsBg, Color reservedRoomsBg, Color occupiedRoomsBg, int roomBookingId, String paymentMethod, Color pendingPayment) {
        String html = "<html>";
        html = html + customerName + "<br /><br />";
        html = html + contactNumber + "<br /><br />";
        html = html + vehicleNumber + "<br /><br />";
        html = html + "</html>";

        String choice = "";
        JLabel label = new JLabel();
        label.setText(html);
        label.setHorizontalAlignment(JLabel.CENTER);

        panelScroll.add(label);
        label.setBounds(x, y, 100 * numberOfDays, 99);
        label.setOpaque(true);

        if (isLeft == 1 && (!(paymentMethod.equals("PENDING")))) {
            label.setBackground(leftRoomsBg);
            choice = "GENERATE_BILL_AGAIN";
            
        } else if (isLeft == 0 && arrivalDateTime.equals("")) {
            label.setBackground(reservedRoomsBg);
            choice = "CANCEL_OR_OCCUPY";
            
        } else if(isLeft == 0 && (!(arrivalDateTime.equals("")))){
            label.setBackground(occupiedRoomsBg);
            choice = "GENERATE_BILL";

        } else if(isLeft == 1 && paymentMethod.equals("PENDING")) {
            label.setBackground(pendingPayment);
            choice = "CLOSE_BILL";
            
        }

        label.addMouseListener(new MyMouseListener(roomBookingId, choice, panelScroll, mainForm, allReservationModified));
    }

}
