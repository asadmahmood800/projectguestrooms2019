/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entity.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author faraz
 */
public class UserService {

    public User tryLogin(User user) {
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String query = "SELECT `id`, `display_name`, `user_name`, `password`, \n"
                + "COUNT(*) AS `count` FROM `users` WHERE \n"
                + "`user_name`='" + user.getUserName() + "' AND `password`='"
                + user.getPassword() + "' \n"
                + "AND `is_active` = 1;";
        int count = 0;
        ResultSet resultSet;
        try {
            resultSet = sql.executeQuery(query);
            resultSet.next();
            count = resultSet.getInt("count");

            if (count == 1) {
                user.setDisplayName(resultSet.getString("display_name"));
                user.setIsActive(1);
                user.setUserId(resultSet.getInt("id"));

            } else {
                user.setIsActive(0);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return user;

    }

    public int registerUser(User user) {
        int status = 0;

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String queryCheck = "SELECT COUNT(*) AS `count` FROM `users`\n" +
"WHERE `user_name` LIKE('" + user.getUserName() + "');";
        String query = "INSERT INTO `users`(`display_name`, `user_name`, `password`, "
                + "`is_active`) VALUES ('"
                + user.getDisplayName() + "', '"
                + user.getUserName() + "', '"
                + user.getPassword() + "', 1);";
        
        ResultSet resultSet;
        try {
            resultSet = sql.executeQuery(queryCheck);
            resultSet.next();
            if(resultSet.getInt("count") == 0) {
                status = sql.executeUpdate(query);
            sql.commit();
            } else {
                status = 2;
            }
                        
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return status;
    }

    public void updateUser(User user) {
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String queryCheck = "SELECT COUNT(*) AS `count` FROM `users` " +
"WHERE `user_name` LIKE('" + user.getUserName() + "') AND `id` != "+user.getUserId()+ ";";
        String queryUpdate = "";
        
        try {
            ResultSet resultSet = sql.executeQuery(queryCheck);
            resultSet.next();
            int count = resultSet.getInt("count");
            
            if(count == 0) {
                queryUpdate = "UPDATE `users` SET `display_name` = '"
                        +user.getDisplayName()+ "', `user_name`='"
                        +user.getUserName()+ "', `is_active` = "
                        +user.getIsActive()+ " WHERE `id` = " +user.getUserId() + ";";
                sql.executeUpdate(queryUpdate);
                
                if(!(user.getPassword().equals(""))) {
                    queryUpdate = "UPDATE `users` SET `password` = '"
                        +user.getPassword()+ "' WHERE `id` = " +user.getUserId() + ";";
                    sql.executeUpdate(queryUpdate);
                }
                
                sql.commit();
                JOptionPane.showMessageDialog(null, "User updated successfully.");
            } else {
                //user name can not be duplicated
            }
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        
    }
    
    
}
