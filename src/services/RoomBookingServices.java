/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import entity.RoomBooking;
import entity.User;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author shad
 */
public class RoomBookingServices {

    public void makeReservation(RoomBooking roomBooking, User user) {

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int userId = user.getUserId();

        String roomId = roomBooking.getRoomId();
        String cnic = roomBooking.getCnic();
        String customerName = roomBooking.getCustomerName();
        String mobileNumber = roomBooking.getMobileNumber();
        String address = roomBooking.getAddress();

        String fromDate = roomBooking.getFromDate();
        String toDate = roomBooking.getToDate();

        String queryDateDifference = "SELECT DATEDIFF('"
                + toDate + "', '" + fromDate + "') AS `difference`;";

        ResultSet resultDifference;
        int difference = -1;
        String queryCheck = "SELECT COUNT(*) AS `count` FROM `room_booking` WHERE\n"
                + "`room_id`=" + roomId + " AND `is_cancelled` = 0 AND\n"
                + "(\n"
                + "    (\n"
                + "    	(`from_date` BETWEEN '" + fromDate + "' AND '" + toDate + "')\n"
                + "    	OR (`to_date` BETWEEN '" + fromDate + "' AND '" + toDate + "')\n"
                + "    )\n"
                + "	OR\n"
                + "    (\n"
                + "        ('" + fromDate + "' BETWEEN `from_date` AND `to_date`)\n"
                + "        OR\n"
                + "        ('" + toDate + "' BETWEEN `from_date` AND `to_date`)\n"
                + "     )\n"
                + "    \n"
                + ");";

        String queryInsert = "";
        ResultSet resultSet;
        int count = 1;
        try {
            resultDifference = sql.executeQuery(queryDateDifference);
            resultDifference.next();
            difference = resultDifference.getInt("difference");

            if (difference < 0) {
                JOptionPane.showMessageDialog(null, "From date can not be greater than to date");
            } else {
                resultSet = sql.executeQuery(queryCheck);
                resultSet.next();
                count = resultSet.getInt("count");

                if (count == 0) {
                    queryInsert = "INSERT INTO `room_booking`(`room_id`, `cnic`, `customer_name`, `address`, \n"
                            + "`mobile_number`, `from_date`, `to_date`, `payment_method`, `inserted_by_user_id`, \n"
                            + "`insertion_date_time`) \n" + "VALUES ("
                            + roomId + ", '" + cnic + "','" + customerName + "','" + address + "','"
                            + mobileNumber + "','" + fromDate + "', '" + toDate + "','PENDING', " + userId + ", `getCurrentDateTime`()" + ");";
                    sql.executeUpdate(queryInsert);
                    sql.commit();
                    JOptionPane.showMessageDialog(null, "Reservation has been made successfully.");
                } else {
                    JOptionPane.showMessageDialog(null, "The selected room has already been reserved in the provided dates by another customer.");
                }
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
    }

    public void cancelReservation(int bookingId, User user) {
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int userId = user.getUserId();

        String query = "UPDATE `room_booking` SET `is_cancelled` = 1, \n"
                + "`cancelled_by_user_id` = " + userId
                + ", `cancellation_date_time` = `getCurrentDateTime`() WHERE `id` = " + bookingId + ";";

        try {
            sql.executeUpdate(query);
            sql.commit();
            JOptionPane.showMessageDialog(null, "Room reservation has been canceled successfully.");
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

    }

    public RoomBooking getRoomBookingDetails(int roomBookingId) {
        RoomBooking roomBooking = new RoomBooking();;

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String query = "SELECT (DATEDIFF(`rb`.`to_date`, `rb`.`from_date`) + 1) AS `number_of_days`, `r`.`per_day_charges`, `r`.`room_number`, `rb`.* FROM `rooms` AS `r`,\n"
                + "`room_booking` AS `rb` WHERE `r`.`id` = `rb`.`room_id` \n"
                + "AND `rb`.`id` = " + roomBookingId + ";";

        String queryFood = "SELECT IFNULL(SUM(`total_price`), 0) AS `total_food` FROM\n"
                + "`room_food_order_details` WHERE `room_booking_id` = '" + roomBookingId + "';";

        ResultSet resultSet;
        ResultSet resultSetFood;

        int perDayCharges = 0;
        try {
            resultSet = sql.executeQuery(query);
            resultSet.next();

            roomBooking.setId(roomBookingId);
            roomBooking.setRoomNumber(resultSet.getString("room_number"));
            roomBooking.setCustomerName(resultSet.getString("customer_name"));
            roomBooking.setCnic(resultSet.getString("cnic"));
            roomBooking.setMobileNumber(resultSet.getString("mobile_number"));
            roomBooking.setVehicleNumber(resultSet.getString("vehicle_number"));
            roomBooking.setNumberOfDays(resultSet.getInt("number_of_days"));
            roomBooking.setFromDate(resultSet.getString("from_date"));
            roomBooking.setToDate(resultSet.getString("to_date"));
perDayCharges = resultSet.getInt("per_day_charges");

            resultSetFood = sql.executeQuery(queryFood);
            resultSetFood.next();
            
            roomBooking.setTotalFoodAmount(resultSetFood.getInt("total_food"));
            roomBooking.setTotalRoomRent(roomBooking.getNumberOfDays() * perDayCharges);
            roomBooking.setGrandTotal(roomBooking.getNumberOfDays() * perDayCharges + roomBooking.getTotalFoodAmount());
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }
        return roomBooking;
    }
    
    
    
      
    public void printSaleReport(String from, String to, String paymentMethodId) {
        
        String query = "SELECT (DATEDIFF(`rb`.`to_date`, `rb`.`from_date`) + 1) AS `number_of_days`, `r`.`per_day_charges`, `r`.`room_number`, `rb`.* FROM `rooms` AS `r`, \n"
                +"`room_booking` AS `rb` WHERE `r`.`id` = `rb`.`room_id` \n"
                + "AND DATE_FORMAT(`rb`.`departure_date_time`, '%Y-%m-%d') BETWEEN '"
                +from+ "' AND '"
                +to+ "' \n"
                + "AND `payment_method`='"
                +paymentMethodId+ "' \n"
                +"ORDER BY `from_date` ASC;";
        
        try {

            Document d = new Document();
            String pdfNameDate = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            PdfWriter.getInstance(d, new FileOutputStream("src/reports/cash_Sale" + pdfNameDate + ".pdf"));
            d.open();

            Paragraph header = new Paragraph("Room Reservation", FontFactory.getFont(FontFactory.TIMES_BOLD, 20, Font.BOLD, BaseColor.BLUE));
            header.setAlignment(Element.ALIGN_CENTER);
            d.add(header);

            Image image = Image.getInstance(getClass().getResource("/images/Logo.png"));

            image.scaleAbsolute(100, 100);
            image.setAbsolutePosition(30, 730);
            Paragraph p = new Paragraph();
            p.setAlignment(Element.ALIGN_CENTER);
            p.add(image);
            d.add(p);

            Paragraph phone = new Paragraph("Phone# : +92316-1217680");
            phone.setAlignment(Element.ALIGN_CENTER);
            d.add(phone);

            Paragraph date = new Paragraph("Date : " + new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(Calendar.getInstance().getTime()));
            date.setAlignment(Element.ALIGN_CENTER);
            d.add(date);

            Paragraph space = new Paragraph("         ");
            space.setAlignment(Element.ALIGN_CENTER);
            d.add(space);

            Paragraph space1 = new Paragraph("         ");
            space1.setAlignment(Element.ALIGN_CENTER);
            d.add(space1);
            
            Paragraph space2 = new Paragraph("         ");
            space2.setAlignment(Element.ALIGN_CENTER);
            d.add(space2);

            PdfPTable t = new PdfPTable(7);
            Font fontH1 = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
            
           
            PdfPCell cell = new PdfPCell(new Paragraph( paymentMethodId +"  "+ "SALE REPORT", FontFactory.getFont(FontFactory.TIMES_BOLD, 14, Font.BOLD,BaseColor.WHITE)));
            cell.setColspan(8);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.BLUE);
            t.addCell(cell);
            
            
            //t.addCell(new PdfPCell(new Phrase("S.N.O", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Room No", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Customer", fontH1)));
            t.addCell(new PdfPCell(new Phrase("From", fontH1)));
            t.addCell(new PdfPCell(new Phrase("To", fontH1)));
            t.addCell(new PdfPCell(new Phrase("No Of Days", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Total", fontH1)));
            t.addCell(new PdfPCell(new Phrase("Payment", fontH1)));
            t.setWidths(new int[]{5,7,7,7,7,7,7});
            
            //result set loop starts
            SQLQueryUtil sql = new SQLQueryUtil();
            sql.connect(false);
            
            ResultSet resultSet;
            RoomBooking roomBooking;
            try {
                resultSet = sql.executeQuery(query);
                while (resultSet.next()) { 
                     
                     t.addCell(resultSet.getString("room_number"));
                     t.addCell(resultSet.getString("customer_name"));
                     t.addCell(resultSet.getString("from_date"));
                     t.addCell(resultSet.getString("to_date"));
                     t.addCell(resultSet.getString("number_of_days"));
                     t.addCell(resultSet.getString("total_bill"));
                     t.addCell(resultSet.getString("payment_method"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            //result set loop ends
            
            d.add(t);
            d.close();

            File pdfFile = new File("src/reports/cash_Sale" + pdfNameDate + ".pdf");
            if (pdfFile.exists()) {
                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(pdfFile);
                    System.out.println("File Has Been Opened Successfully!");
                } else {
                    System.out.println("Desktop Not Supported!");
                }
            } else {
                System.out.println("PDF File Is Not Generated");
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        
    }
    
    
    
    
    
  
    
}
