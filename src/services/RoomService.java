/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entity.Rooms;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JOptionPane;
import util.SQLQueryUtil;

/**
 *
 * @author ASAD MAHMOOD
 */
public class RoomService {

    private Rooms room;

    public int addRoom(Rooms room) {
        int status = 0;

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String queryCheck = "SELECT COUNT(*) AS `count` FROM `rooms` " + "WHERE `room_number` LIKE ('"
                + room.getRoomNumber() + "');";

        String query = "INSERT INTO `rooms`(`room_types_id`, `room_number`, `inserted_by_user_id`, `insertion_date_time`, `is_out_of_order`, `per_day_charges`)"
                + " VALUES ("
                + room.getRoomTypeId() + ",'"
                + room.getRoomNumber() + "'"
                + ",1, `getCurrentDateTime`(), 0, '"
                + room.getPerDayCharges() + "');";

        ResultSet resultSet;
        try {
            resultSet = sql.executeQuery(queryCheck);
            resultSet.next();
            if (resultSet.getInt("count") == 0) {
                status = sql.executeUpdate(query);
                sql.commit();

            } else {
                status = 2;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();
        }

        return status;

    }

    public void updateRoom(Rooms room) {
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String queryCheck = "SELECT COUNT(*) AS `count` FROM `rooms` \n"
                + "WHERE `room_number` LIKE ('" + room.getRoomNumber() + "') AND `id` !='" + room.getRoomId() + "'";

        String queryUpdate = "";

        try {
            ResultSet resultSet = sql.executeQuery(queryCheck);
            resultSet.next();
            int count = resultSet.getInt("count");

            if (count == 0) {
                queryUpdate = "UPDATE `rooms` SET `room_types_id` = '"
                        + room.getRoomTypeId() + "',`room_number` = '"
                        + room.getRoomNumber() + "',`is_out_of_order`='"
                        + room.getIsOutOfOrder() + "',`per_day_charges`='"
                        + room.getPerDayCharges() + "' WHERE `id` = '" + room.getRoomId() + "';";

                sql.executeUpdate(queryUpdate);
                sql.commit();
                JOptionPane.showMessageDialog(null, "Room updated successfully.");
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            sql.disconnect();

        }

    }

    public Vector<Rooms> getVectorInOrderRoom() {
        Vector<Rooms> vectorInOrderRoom = new Vector<>();

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String query = "SELECT `rt`.`types`, `r`.`id`, `r`.`room_number` FROM\n"
                + "`rooms_types` AS `rt`, `rooms` AS `r` WHERE\n"
                + "`rt`.`id` = `r`.`room_types_id` \n"
                + "AND `r`.`is_out_of_order` = 0\n"
                + "ORDER BY `r`.`room_number` ASC;";

        ResultSet resultSet;
        Rooms room;

        try {
            resultSet = sql.executeQuery(query);
            while (resultSet.next()) {
                room = new Rooms();
                room.setRoomId(resultSet.getInt("id"));
                room.setRoomNumber(resultSet.getString("room_number"));
                room.setRoomType(resultSet.getString("types"));
                room.setRoomTypeLabel(resultSet.getString("types"));
                vectorInOrderRoom.add(room);
                room.setIsOutOfOrder(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();;

        } finally {
            sql.commit();
            sql.disconnect();
        }

        return vectorInOrderRoom;
    }

    public Vector<Rooms> getVectorOutOrderRoom() {
        Vector<Rooms> vectorOutOrderRooms = new Vector<>();

        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);

        String query = "SELECT `rt`.`types`, `r`.`id`, `r`.`room_number` FROM\n"
                + "`rooms_types` AS `rt`, `rooms` AS `r` WHERE\n"
                + "`rt`.`id` = `r`.`room_types_id` \n"
                + "AND `r`.`is_out_of_order` = 1\n"
                + "ORDER BY `r`.`room_number` ASC;";

        ResultSet resultSet;
        Rooms room;

        try {
            resultSet = sql.executeQuery(query);
            while (resultSet.next()) {
                room = new Rooms();
                room.setRoomId(resultSet.getInt("id"));
                room.setRoomNumber(resultSet.getString("room_number"));
                room.setRoomType(resultSet.getString("types"));
                room.setRoomTypeLabel(resultSet.getString("types"));
                room.setIsOutOfOrder(1);
                vectorOutOrderRooms.add(room);
            }
        } catch (Exception ex) {
            ex.printStackTrace();;

        } finally {
            sql.commit();
            sql.disconnect();
        }

        return vectorOutOrderRooms;
    }

    public void changeOrder(Rooms room) {
        String query = "";
        SQLQueryUtil sql = new SQLQueryUtil();
        sql.connect(false);
        int roomOrder = room.getIsOutOfOrder();

        if (roomOrder == 0) {
            query = "UPDATE `rooms` SET `is_out_of_order`= '1' WHERE id ='" + room.getRoomId() + "';";
        } else if (roomOrder == 1) {
            query = "UPDATE `rooms` SET `is_out_of_order`= '0' WHERE id ='" + room.getRoomId() + "';";
        }
        try {
            sql.executeUpdate(query);
            sql.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {

            sql.disconnect();
        }
    }
}
