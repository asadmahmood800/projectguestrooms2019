/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author ASAD MAHMOOD
 */
public class RoomFoodOrder {
    private int roomFoodOrderId;
    private FoodItems foodItem;
    
    private String quantity;
    private String totalPrice;
    
    private RoomBooking roomBooking;
    
    private String bulkId;

    public RoomFoodOrder() {
    }

    public int getRoomFoodOrderId() {
        return roomFoodOrderId;
    }

    public void setRoomFoodOrderId(int roomFoodOrderId) {
        this.roomFoodOrderId = roomFoodOrderId;
    }

    public FoodItems getFoodItem() {
        return foodItem;
    }

    public void setFoodItem(FoodItems foodItem) {
        this.foodItem = foodItem;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public RoomBooking getRoomBooking() {
        return roomBooking;
    }

    public void setRoomBooking(RoomBooking roomBooking) {
        this.roomBooking = roomBooking;
    }

    public String getBulkId() {
        return bulkId;
    }

    public void setBulkId(String bulkId) {
        this.bulkId = bulkId;
    }

    
    
}
