/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author ASAD MAHMOOD
 */
public class RoomBooking {
    
    private int id;
    private String roomId;
    private String cnic;
    private String customerName;
    private String address;
    private String mobileNumber;
    private String fromDate;
    private String toDate;
    private String arrivalDateTime;
    private String departureDateTime;
    private int isLeft;
    private String vehicleNumber;
    private int totalBill;
    private String paymentMethod;
    private int insertedByUserId;
    private String insertionDateTime;
    private int billGeneratedByUserId;
    private int isCancelled;
    private int numberOfOccupants;
    private String chequeNumber;
    private String bankName;
    private String branchName;
    private int cancelledByUserId;
    private String cancellationDateTime;

    private String roomNumber;
    
    private int numberOfDays;
    
    private int totalFoodAmount;
    
    private int totalRoomRent;

    private int grandTotal;

    public int getTotalRoomRent() {
        return totalRoomRent;
    }

    public void setTotalRoomRent(int totalRoomRent) {
        this.totalRoomRent = totalRoomRent;
    }

    public int getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(int grandTotal) {
        this.grandTotal = grandTotal;
    }

    
    public int getTotalFoodAmount() {
        return totalFoodAmount;
    }

    public void setTotalFoodAmount(int totalFoodAmount) {
        this.totalFoodAmount = totalFoodAmount;
    }

    
    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(String arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public String getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(String departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public int getIsLeft() {
        return isLeft;
    }

    public void setIsLeft(int isLeft) {
        this.isLeft = isLeft;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public int getTotalBill() {
        return totalBill;
    }

    public void setTotalBill(int totalBill) {
        this.totalBill = totalBill;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public int getInsertedByUserId() {
        return insertedByUserId;
    }

    public void setInsertedByUserId(int insertedByUserId) {
        this.insertedByUserId = insertedByUserId;
    }

    public String getInsertionDateTime() {
        return insertionDateTime;
    }

    public void setInsertionDateTime(String insertionDateTime) {
        this.insertionDateTime = insertionDateTime;
    }

    public int getBillGeneratedByUserId() {
        return billGeneratedByUserId;
    }

    public void setBillGeneratedByUserId(int billGeneratedByUserId) {
        this.billGeneratedByUserId = billGeneratedByUserId;
    }

    public int getIsCancelled() {
        return isCancelled;
    }

    public void setIsCancelled(int isCancelled) {
        this.isCancelled = isCancelled;
    }

    public int getNumberOfOccupants() {
        return numberOfOccupants;
    }

    public void setNumberOfOccupants(int numberOfOccupants) {
        this.numberOfOccupants = numberOfOccupants;
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public int getCancelledByUserId() {
        return cancelledByUserId;
    }

    public void setCancelledByUserId(int cancelledByUserId) {
        this.cancelledByUserId = cancelledByUserId;
    }

    public String getCancellationDateTime() {
        return cancellationDateTime;
    }

    public void setCancellationDateTime(String cancellationDateTime) {
        this.cancellationDateTime = cancellationDateTime;
    }

    public RoomBooking() {
    }
    
    @Override
    public String toString(){
        return roomId;
    }

    public void setRoom_id(RoomBooking roomNumber) {
        
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }
    
    
    
}
