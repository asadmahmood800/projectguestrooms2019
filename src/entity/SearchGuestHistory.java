///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package entity;
//
///**
// *
// * @author ASAD MAHMOOD
// */
//public class SearchGuestHistory {
//    
//    private String roomNumber;
//    private String noOfDays;
//    private int perDayCharges;
//    private String customerName;
//    private String cnic;
//    private String moblieNumber;
//    private String  address;
//    
//    private RoomBooking roomBooking;
//
//    public SearchGuestHistory() {
//    }
//
//    public SearchGuestHistory(String roomNumber, String noOfDays, String customerName, String cnic, String moblieNumber) {
//        this.roomNumber = roomNumber;
//        this.noOfDays = noOfDays;
//        this.customerName = customerName;
//        this.cnic = cnic;
//        this.moblieNumber = moblieNumber;
//    }
//
//    
//    
//    public String getRoomNumber() {
//        return roomNumber;
//    }
//
//    public void setRoomNumber(String roomNumber) {
//        this.roomNumber = roomNumber;
//    }
//
//    public String getNoOfDays() {
//        return noOfDays;
//    }
//
//    public void setNoOfDays(String noOfDays) {
//        this.noOfDays = noOfDays;
//    }
//
//    public int getPerDayCharges() {
//        return perDayCharges;
//    }
//
//    public void setPerDayCharges(int perDayCharges) {
//        this.perDayCharges = perDayCharges;
//    }
//    
//    
//
//    public String getCustomerName() {
//        return customerName;
//    }
//
//    public void setCustomerName(String customerName) {
//        this.customerName = customerName;
//    }
//
//    public String getCnic() {
//        return cnic;
//    }
//
//    public void setCnic(String cnic) {
//        this.cnic = cnic;
//    }
//
//    public String getMoblieNumber() {
//        return moblieNumber;
//    }
//
//    public void setMoblieNumber(String moblieNumber) {
//        this.moblieNumber = moblieNumber;
//    }
//    
//    public String getAddress() {
//        return address;
//    }
//    
//    public void setAddress(String address) {
//        this.address = address;
//    }
//    
//    
//}
