/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author ASAD MAHMOOD
 */
public class RoomBills {
    
    private int id;
    private int roombookingId;
    private String title;
    private int rate;
    private int quantity;
    private int totalAmount;

    public RoomBills() {
    }

    public RoomBills(int id, int roombookingId, String title, int rate, int quantity, int totalAmount) {
        this.id = id;
        this.roombookingId = roombookingId;
        this.title = title;
        this.rate = rate;
        this.quantity = quantity;
        this.totalAmount = totalAmount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoombookingId() {
        return roombookingId;
    }

    public void setRoombookingId(int roombookingId) {
        this.roombookingId = roombookingId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    
    
}
