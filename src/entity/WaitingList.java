/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.toedter.calendar.JDateChooser;
import java.util.Date;

/**
 *
 * @author ASAD MAHMOOD
 */
public class WaitingList {

    public WaitingList() {
    }
    
    private int waitingListId;
    private String customerName;
    private String cnic;
    private String address;
    private String phoneNumber;
    private String fromDate;
    private String toDate;
    private int insertedByUserId;
    private String insertionDateTime;
    private int isCancelled;
    private int isRoomAllotted;

    public int getWaitingListId() {
        return waitingListId;
    }

    public void setWaitingListId(int waitingListId) {
        this.waitingListId = waitingListId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public int getInsertedByUserId() {
        return insertedByUserId;
    }

    public void setInsertedByUserId(int insertedByUserId) {
        this.insertedByUserId = insertedByUserId;
    }

    public String getInsertionDateTime() {
        return insertionDateTime;
    }

    public void setInsertionDateTime(String insertionDateTime) {
        this.insertionDateTime = insertionDateTime;
    }

    public int getIsCancelled() {
        return isCancelled;
    }

    public void setIsCancelled(int isCancelled) {
        this.isCancelled = isCancelled;
    }

    public int getIsRoomAllotted() {
        return isRoomAllotted;
    }

    public void setIsRoomAllotted(int isRoomAllotted) {
        this.isRoomAllotted = isRoomAllotted;
    }

    public JDateChooser getToDate(Date dateto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
