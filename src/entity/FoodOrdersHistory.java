package entity;

/**
 *
 * @author ASAD MAHMOOD
 */
public class FoodOrdersHistory {
    
    private String roomBookingId;
    private String foodItemId;
    private String unitPrice;
    private String toatalPrice;
    
    private String quantity;
    private String foodItemName;
    private String customerName;
    private String mobileNumber;
    private String cnic;
    private String roomNumber;
    
    public FoodOrdersHistory () {
      
    }
  
    public String getRoomBookingId() {
        return roomBookingId;
    }

    public void setRoomBookingId(String roomBookingId) {
        this.roomBookingId = roomBookingId;
    }

    public String getFoodItemId() {
        return foodItemId;
    }

    public void setFoodItemId(String foodItemId) {
        this.foodItemId = foodItemId;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getToatalPrice() {
        return toatalPrice;
    }

    public void setToatalPrice(String toatalPrice) {
        this.toatalPrice = toatalPrice;
    }  

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getFoodItemName() {
        return foodItemName;
    }

    public void setFoodItemName(String foodItemName) {
        this.foodItemName = foodItemName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }
    
    

    
}
