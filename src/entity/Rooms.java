/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author ASAD MAHMOOD
 */
public class Rooms {
    private int roomId;
    private String roomNumber;
    private String roomType;
    private int perDayCharges;
    private int isOutOfOrder;

    private int roomTypeId;
    private String roomTypeLabel;
    
    public Rooms() {
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }
    
    
    
    public int getIsOutOfOrder() {
        return isOutOfOrder;
    }

    public void setIsOutOfOrder(int isOutOfOrder) {
        this.isOutOfOrder = isOutOfOrder;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public int getPerDayCharges() {
        return perDayCharges;
    }

    public void setPerDayCharges(int perDayCharges) {
        this.perDayCharges = perDayCharges;
    }

    public int getRoomTypeId() {
        return roomTypeId;
    }

    public void setRoomTypeId(int roomTypeId) {
        this.roomTypeId = roomTypeId;
    }

    public String getRoomTypeLabel() {
        return roomTypeLabel;
    }

    public void setRoomTypeLabel(String roomTypeLabel) {
        this.roomTypeLabel = roomTypeLabel;
    }

    @Override
    public String toString() {
        return roomNumber;
    }

}
