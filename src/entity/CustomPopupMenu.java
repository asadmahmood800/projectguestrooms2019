package entity;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import util.SQLQueryUtil;
import views.AllReservationsModified;
import views.CloseBillForm;
import views.GenerateBillForm;
import views.MainForm;
import views.OccupyRoomForm;

public class CustomPopupMenu {
    
    private MainForm mainForm;
    private OccupyRoomForm occupyRoomForm;
    private GenerateBillForm billGeneratedForm;
    private CloseBillForm closeBillForm;
    

    private JPopupMenu menu;

    private JMenuItem pmiCancelReservation;
    private JMenuItem pmiOccupyRoom;
    private JMenuItem pmiGenerateBill;
    private JMenuItem pmiGenerateBillAgain;
    
    private JMenuItem pmiCloseBillAgain;

    private String choice;

    private int roomBookingId;

    private JPanel panel;
    
    private AllReservationsModified allReservationModified;
    /**
     *
     * @param choice if choice is CANCE_OR_OCCUPY THEN we will show two menu
     * items cancel reservation occupy room
     *
     * if choice is GENERATE_BILL THEN we will show one menu item i.e generate
     * bill
     *
     * if choice is GENERATE_BILL_AGAIN THEN we will show one menu item i.e
     * generate bill Again
     *
     *
     *
     *
     */
    
    /**
     * 
     * @param choice
     * @param roomBookingId
     * @param panel 
     */
    public CustomPopupMenu(String choice, int roomBookingId, JPanel panel, MainForm mainForm) {
        this.choice = choice;
        this.roomBookingId = roomBookingId;
        this.panel = panel;
        
        menu = new JPopupMenu();

        pmiCancelReservation = new JMenuItem("Cancel Reservation");
        pmiOccupyRoom = new JMenuItem("Occupy Room");
        pmiGenerateBill = new JMenuItem("Generate Bill");
        pmiGenerateBillAgain = new JMenuItem("Generate Bill Again");
        
        pmiCloseBillAgain = new JMenuItem("Close Bill");

        if (choice.equals("CANCEL_OR_OCCUPY")) {
            menu.add(pmiOccupyRoom);
            menu.add(pmiCancelReservation);

        } else if (choice.equals("GENERATE_BILL")) {
            menu.add(pmiGenerateBill);

        } else if (choice.equals("GENERATE_BILL_AGAIN")) {
            menu.add(pmiGenerateBillAgain);
            
        } else if(choice.equals("CLOSE_BILL")) {
            menu.add(pmiCloseBillAgain);
        }

        
        pmiCloseBillAgain.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                
                int choice = -1;
                choice = JOptionPane.showConfirmDialog(null, "Are you sure you want to close this bill?");
                
                if(choice == 0) {
                    allReservationModified.dispose();
                    
                    closeBillForm = new CloseBillForm(mainForm, roomBookingId);
                    mainForm.add(closeBillForm);
                    closeBillForm.setVisible(true);
                 }
                
            }
        });
                
                
        pmiCancelReservation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                //JOptionPane.showMessageDialog(null, "cancel reservation. Booking id = " + roomBookingId);
                int choice = -1;
                choice = JOptionPane.showConfirmDialog(null, "Do you want to cancel room reservation...!");
                
                if(choice == 0){
                    SQLQueryUtil sql = new SQLQueryUtil();
                    sql.connect(false);
                    
                    String query = "UPDATE `room_booking` SET `is_cancelled`= '1'"
                            + " WHERE `id` = '"
                            + roomBookingId + "';";
                    try {
                        sql.executeUpdate(query);
                        sql.commit();
                        JOptionPane.showMessageDialog(null, "Room is cancelled successfully...!");
                        allReservationModified.dispose();
                        allReservationModified = new AllReservationsModified(mainForm);
                        mainForm.add(allReservationModified);
                        allReservationModified.setVisible(true);
                        
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        
        pmiOccupyRoom.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                
                int choice = -1;
                choice = JOptionPane.showConfirmDialog(null, "Are you sure you want to Occupy this Room?");
                
                if(choice == 0) {
                    allReservationModified.dispose();
                    
                    occupyRoomForm = new OccupyRoomForm(mainForm, roomBookingId);
                    mainForm.add(occupyRoomForm);
                    occupyRoomForm.setVisible(true);
                 }
                
            }
        });
        
        
        pmiGenerateBill.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                //JOptionPane.showMessageDialog(null, "generate bill. Booking id = " + roomBookingId);
                
                int choice = -1;
                choice = JOptionPane.showConfirmDialog(null, "Are you sure you want to Generate Bill for this Room?");
                if (choice == 0) {
                    allReservationModified.dispose();
                    billGeneratedForm = new GenerateBillForm(mainForm, roomBookingId);
                    mainForm.add(billGeneratedForm);
                    billGeneratedForm.setVisible(true);
                }
                
            }
        });
        
        pmiGenerateBillAgain.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                //JOptionPane.showMessageDialog(null, "generate bill again. Booking id = " + roomBookingId);
                  int choice = -1;
                  choice = JOptionPane.showConfirmDialog(null, "Are you sure you want to Generate Bill Again.?");
                  if (choice == 0) {
                      allReservationModified.dispose();
                      closeBillForm = new CloseBillForm(mainForm, roomBookingId);
                      mainForm.add(closeBillForm);
                      closeBillForm.setVisible(true);
                  }
            }
        });
        

    }

     //public void showPopup(ActionEvent ae) {
     public void showPopup(MouseEvent ae, AllReservationsModified allReservationModified) {
        // Get the event source
        this.allReservationModified = allReservationModified;
        Component b = (Component) ae.getSource();

        // Get the location of the point 'on the screen'
        Point p = b.getLocationOnScreen();

        menu.show(panel, 0, 0);

        menu.setLocation(p.x, p.y + b.getHeight());
    }
}
