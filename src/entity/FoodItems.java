/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author ASAD MAHMOOD
 */
public class FoodItems {
    
    private int id;
    private String FoodItemName;
    private String FoodItemPrice;
    
    public FoodItems() {
        
    }

    public FoodItems(int id, String FoodItemName, String FoodItemPrice) {
        this.id = id;
        this.FoodItemName = FoodItemName;
        this.FoodItemPrice = FoodItemPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFoodItemName() {
        return FoodItemName;
    }

    public void setFoodItemName(String FoodItemName) {
        this.FoodItemName = FoodItemName;
    }

    public String getFoodItemPrice() {
        return FoodItemPrice;
    }

    public void setFoodItemPrice(String FoodItemPrice) {
        this.FoodItemPrice = FoodItemPrice;
    }
    
    @Override
    public String toString() {
        return FoodItemName;
    }
    
    
    
}
