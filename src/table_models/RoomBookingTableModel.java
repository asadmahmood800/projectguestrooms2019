/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.RoomBooking;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shad
 */
public class RoomBookingTableModel extends AbstractTableModel {

    private Vector<RoomBooking> vectorRoomBooking;

    public RoomBookingTableModel(Vector<RoomBooking> vectorRoomBooking) {
        this.vectorRoomBooking = vectorRoomBooking;
    }

    @Override
    public int getRowCount() {
        return vectorRoomBooking.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String columnName = "";
        switch (columnIndex) {
            case 0:
                columnName = "ID";
                break;
            case 1:
                columnName = "Room Number";
                break;
            case 2:
                columnName = "Customer Name";
                break;
            case 3:
                columnName = "CNIC";
                break;
            case 4:
                columnName = "Mobile Number";
                break;
            case 5:
                columnName = "From Date";
                break;
            case 6:
                columnName = "To Date";
                break;
        }
        return columnName;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = null;
        switch (columnIndex) {
            case 0:
                object = vectorRoomBooking.get(rowIndex).getId();
                break;
            case 1:

                object = vectorRoomBooking.get(rowIndex).getRoomId();
                break;

            case 2:
                object = vectorRoomBooking.get(rowIndex).getCustomerName();
                break;
            case 3:
                object = vectorRoomBooking.get(rowIndex).getCnic();
                break;
            case 4:
                object = vectorRoomBooking.get(rowIndex).getMobileNumber();
                break;

            case 5:
                object = vectorRoomBooking.get(rowIndex).getFromDate();
                break;
            case 6:
                object = vectorRoomBooking.get(rowIndex).getToDate();
                break;
        }

        return object;
    }

    public Vector<RoomBooking> getVectorRoomBooking() {
        return vectorRoomBooking;
    }

    
}
