/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.Rooms;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ASAD MAHMOOD
 */
public class RoomsTableModel extends AbstractTableModel {

    private Vector<Rooms> vectorRooms;

    public RoomsTableModel(Vector<Rooms> vectorRooms) {
        this.vectorRooms = vectorRooms;
    }

    public Vector<Rooms> getVectorRooms() {
        return vectorRooms;
    }

    @Override
    public int getRowCount() {
        return vectorRooms.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String columnName = "";

        switch (columnIndex) {
            case 0:
                columnName = "ID";
                break;
            case 1:
                columnName = "Room Type";
                break;
            case 2:
                columnName = "Room Number";
                break;
            case 3:
                columnName = "Per Day Charges";
                break;
            case 4:
                columnName = "Current Status";
                break;

        }

        return columnName;

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = null;

        switch (columnIndex) {

            case 0:
                object = vectorRooms.get(rowIndex).getRoomId();
                break;
            case 1:
                object = vectorRooms.get(rowIndex).getRoomTypeLabel();
                break;
            case 2:
                object = vectorRooms.get(rowIndex).getRoomNumber();
                break;
            case 3:
                object = vectorRooms.get(rowIndex).getPerDayCharges();
                break;
            case 4:
                if(vectorRooms.get(rowIndex).getIsOutOfOrder() == 1) {
                    object = "Not Available";
                } else {
                    object = "Available";
                }
                break;

        }

        return object;
    }
}
