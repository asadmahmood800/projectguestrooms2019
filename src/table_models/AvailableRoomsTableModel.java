/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author faraz
 */
public class AvailableRoomsTableModel extends AbstractTableModel {
    
    

    @Override
    public int getRowCount() {
        
        return 20;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }
    
    @Override
    public String getColumnName(int columnIdex) {
        String columnName = "";
        
        switch(columnIdex) {
            case 0:
                columnName = "ID";
                break;
            case 1:
                columnName = "Room Type";
                break;
            case 2:
                columnName = "Room Number";
                break;
            case 3:
                columnName = "Per Day Charges";
                break;
        }
        
        return columnName;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = null;
        return object;
    }
    
}
