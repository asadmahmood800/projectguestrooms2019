/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.User;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Suleman Ahad
 */
public class UserTableModel extends AbstractTableModel {
    private Vector<User> vectorUsers;

    public UserTableModel(Vector<User> vectorUsers) {
        this.vectorUsers = vectorUsers;
    }

    public Vector<User> getVectorUsers() {
        return vectorUsers;
    }

    
    @Override
    public int getRowCount() {
        return vectorUsers.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String columnName = "";
        
        switch(columnIndex) {
            case 0:
                columnName = "ID";break;
            case 1:
                columnName = "Display Name";break;
            case 2:
                columnName = "Username";break;
            case 3:
                columnName = "Is Active";break;
            
        }

        return columnName;

    }
 
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = null;
        
        switch(columnIndex) {
            case 0:
                object = vectorUsers.get(rowIndex).getUserId();
                break;
                
            case 1:
                object = vectorUsers.get(rowIndex).getDisplayName();
                break;
                
            case 2:
                object = vectorUsers.get(rowIndex).getUserName();
                break;
            case 3:
                if(vectorUsers.get(rowIndex).getIsActive() == 1) {
                object = "Active";
                } else {
                    object = "Disabled";
                }
                
                break;
        }

        return object;

    }
    
    
}
