/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.RoomFoodOrder;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Suleman
 */
public class RoomFoodOrderTableModel extends AbstractTableModel {

    private Vector<RoomFoodOrder> vectorRoomFoodOrder;

    public RoomFoodOrderTableModel(Vector<RoomFoodOrder> vectorRoomFoodOrder) {
        this.vectorRoomFoodOrder = vectorRoomFoodOrder;
    }

    @Override
    public int getRowCount() {
        System.out.println("this is get row count method");
        return vectorRoomFoodOrder.size();

    }

    @Override
    public int getColumnCount() {
        System.out.println("this is get column count method");
        return 3;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String columnName = "";
        switch (columnIndex) {
            case 0:
                columnName = "Food Item";
                break;
            case 1:
                columnName = "Quantity";
                break;
            case 2:
                columnName = "Total Price";
                break;
        }

        return columnName;

    }

    @Override
    public Object getValueAt(int rowIndex, int coloumnIndex) {
                System.out.println("row index = " + rowIndex + "...." + "column index = " + coloumnIndex);
        Object object = null;
        switch (coloumnIndex) {
            case 0:
                object = vectorRoomFoodOrder.get(rowIndex).getFoodItem().getFoodItemName();
                break;
            case 1:
                object = vectorRoomFoodOrder.get(rowIndex).getQuantity();
                break;
            case 2:
                object = vectorRoomFoodOrder.get(rowIndex).getTotalPrice();
                break;

        }

        return object;

    }

}
