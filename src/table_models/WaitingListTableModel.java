/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.WaitingList;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shad
 */
public class WaitingListTableModel extends AbstractTableModel {

    private Vector<WaitingList> vectorWaitingList;

    public WaitingListTableModel(Vector<WaitingList> vectorWaitingList) {
        this.vectorWaitingList = vectorWaitingList;
    }

    public Vector<WaitingList> getVectorWaitingList() {
        return vectorWaitingList;
    }
    
    
    
    @Override
    public int getRowCount() {
        return vectorWaitingList.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String columnName = "";
        
        switch(columnIndex) {
            case 0:
                columnName = "Customer Name";break;
            case 1:
                columnName = "CNIC";break;
            case 2:
                columnName = "Mobile Number";break;
            case 3:
                columnName = "From Date";break;
            case 4:
                columnName = "To Date";break;
        }
        return columnName;
    }
    
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
       Object object = null;
       switch(columnIndex) {
            case 0:
                object = vectorWaitingList.get(rowIndex).getCustomerName();
                break;
                
            case 1:
                object = vectorWaitingList.get(rowIndex).getCnic();
                break;
                
            case 2:
                object = vectorWaitingList.get(rowIndex).getPhoneNumber();
                break;
            case 3:
                object = vectorWaitingList.get(rowIndex).getFromDate();
                break;
            case 4:
                object = vectorWaitingList.get(rowIndex).getToDate();
                break;
       }
       
       return object; 
    }
    
    
    
}
