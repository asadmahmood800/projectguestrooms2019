/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.Rooms;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ASAD MAHMOOD
 */

    public class OutOrderRoomsTableModel extends AbstractTableModel{
    private Vector<Rooms> vectorOutOrderRooms;

    public OutOrderRoomsTableModel(Vector<Rooms> vectorOutOrderRooms) {
        this.vectorOutOrderRooms = vectorOutOrderRooms;
    }

    public Vector<Rooms> getVectorOutOrderRooms() {
        return vectorOutOrderRooms;
    }

    @Override
    public int getRowCount() {
        return vectorOutOrderRooms.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String columnName = "";

        switch (columnIndex) {
            case 0:
                columnName = "ID";
                break;
            case 1:
                columnName = "Room Type";
                break;
            case 2:
                columnName = "Room Number";
                break;            
            

        }

        return columnName;

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = null;

        switch (columnIndex) {

            case 0:
                object = vectorOutOrderRooms.get(rowIndex).getRoomId();
                break;
            case 1:
                object = vectorOutOrderRooms.get(rowIndex).getRoomTypeLabel();
                break;
            case 2:
                object = vectorOutOrderRooms.get(rowIndex).getRoomNumber();
                break;        
            
        }

        return object;
    }
    
}
    

