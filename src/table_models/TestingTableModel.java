/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Suleman
 */
public class TestingTableModel extends AbstractTableModel {

    @Override
    public int getRowCount() {
        return 10;
    }

    @Override
    public int getColumnCount() {
        int columnCount = 12;

        return columnCount;

    }

    @Override
    public String getColumnName(int columnIndex) {
        String columnName = "";
        String columnNamesArray[] = {
            "Room Number/DATE", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
            "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21",
            "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"
        };

        columnName = columnNamesArray[columnIndex];

        return columnName;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = null;
        if (rowIndex == 0 && (columnIndex == 3 || columnIndex == 4)) {
            object = "";
        } else {
            object = rowIndex + "..." + columnIndex;
        }

        return object;
    }
    
    

}
