/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.FoodItems;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author faraz
 */
public class FoodItemTableModel extends AbstractTableModel {
    
    private Vector<FoodItems> VectorFoodItem;
    

    public FoodItemTableModel(Vector<FoodItems> VectorFoodItem) {
        this.VectorFoodItem = VectorFoodItem;
    }

    public Vector<FoodItems> getVectorFoodItem() {
        return VectorFoodItem;
    }

    public void setVectorFoodItem(Vector<FoodItems> VectorFoodItem) {
        this.VectorFoodItem = VectorFoodItem;
    }
    
    @Override
    public int getRowCount() {
        return VectorFoodItem.size();
        }
    
    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int columnIndex) {
        
        String columnName = "";
        
        switch(columnIndex) {
            case 0:
                columnName = "ID";
                break;
            case 1:
                columnName = "Food Item Name";
                break;
            case 2:
                columnName = "Price";
                break;
        }
        
        return columnName;
    }

    
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        
        Object object = null;
        
        switch(columnIndex) {
            case 0:
                object = VectorFoodItem.get(rowIndex).getId();
                break;
            case 1:
                object = VectorFoodItem.get(rowIndex).getFoodItemName();
                break;
            case 2:
                object = VectorFoodItem.get(rowIndex).getFoodItemPrice();
        }
        
        return object;
        
    }

    
    
    
    
    
}
