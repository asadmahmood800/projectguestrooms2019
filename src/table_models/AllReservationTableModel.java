/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author shad9619
 */
public class AllReservationTableModel extends AbstractTableModel{

    private String date;
    private int year;
    private int month;
    private int day;
    
    private int isLeapYear;
    private int isFebruary;
    
    private String dateArray[];
    
    public AllReservationTableModel(String date) {
        this.date = date;
        dateArray = date.split("-");
        year = Integer.parseInt(dateArray[0]);
        month = Integer.parseInt(dateArray[1]);
        day = Integer.parseInt(dateArray[2]);
        
        if(month == 2) {
            isFebruary = 1;
        }
        
        if((year % 4) == 0) {
            isLeapYear = 1;
        }
    }
    
    
    @Override
    public int getRowCount() {
        return 10;
    }

    @Override
    public int getColumnCount() {
        int columnCount = 29;
        
        if(isLeapYear == 1 && isFebruary == 1) {
            columnCount = 30;
        } else if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10  || month == 12) {
            columnCount = 32;
        } else if(month == 4 || month == 6 || month == 9 || month == 11) {
            columnCount = 31;
        }
        
        return columnCount;

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = null;
        return object;
    }
    
     @Override
    public String getColumnName(int columnIndex) {
        String columnName = "";
        
        switch(columnIndex) {
            case 0:
                columnName = "Room Number/DATE";
                break;
            
            case 1:
                columnName = "1";
                break;
                
            case 2:
                columnName = "2";
                break;
                
            case 3:
                columnName = "3";
                break;
            case 4:
                columnName = "4";
                break;
            case 5:
                columnName = "5";
                break;
            case 6: 
                columnName = "6";
                break; 
            case 7:
                columnName = "7";
                break;  
            case 8:
                columnName = "8";
                break;
            case 9:
                columnName = "9";
                break;
            case 10:
                columnName = "10";
                break;
            case 11:
                columnName = "11";
                break;
            case 12:
                columnName = "12";
                break;
            case 13:
                columnName = "13";
                break;  
            case 14:
                columnName = "14";
                break;
            case 15:
                columnName = "15";
                break;
            case 16:
                columnName = "16";
                break;
            case 17:
                columnName = "17";
                break;
            case 18:
                columnName = "18";
                break;
            case 19:
                columnName = "19";
                break;
            case 20:
                columnName = "20";
                break;
            case 21:
                columnName = "21";
                break;
            case 22:
                columnName = "22";
                break;
            case 23:
                columnName = "23";
                break;
            case 24:
                columnName = "24";
                break;
            case 25:
                columnName = "25";
                break;
            case 26:
                columnName = "26";
                break;
            case 27:
                columnName = "27";
                break;
            case 28:
                columnName = "28";
                break;
            case 29:
                columnName = "29";
                break;
            case 30:
                columnName = "30";
                break;
            case 31:
                columnName = "31";
                break;      
                  
        }

        return columnName;

    }
    
    
}
