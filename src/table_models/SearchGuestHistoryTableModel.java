/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.RoomBooking;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author faraz
 */
public class SearchGuestHistoryTableModel extends AbstractTableModel{
    
    private Vector<RoomBooking> vectorRoomBooking;

    public SearchGuestHistoryTableModel() {
    }

    public SearchGuestHistoryTableModel(Vector<RoomBooking> vectorRoomBooking) {
        this.vectorRoomBooking = vectorRoomBooking;
    }

    @Override
    public int getRowCount() {
        return vectorRoomBooking.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }
    
    @Override
    public String getColumnName(int columnIdex) {
        
        String columnName = "";
        
        switch (columnIdex) {
            case 0:
                columnName = "Room No";
                break;
            case 1:
                columnName = "From";
                break;
            case 2:
                columnName = "To";
                break;
            case 3:
                columnName = "No Of Days";
                break;
            case 4:
                columnName = "Customer";
                break;
            case 5:
                columnName = "CNIC";
                break;
            case 6:
                columnName = "Mobile No";
                break;
            case 7:
                columnName = "Address";
                break;
        }
        
        return columnName;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = null;
        
        switch(columnIndex) {
            case 0:
                object = vectorRoomBooking.get(rowIndex).getRoomNumber();
                break;
            case 1:
                object = vectorRoomBooking.get(rowIndex).getFromDate();
                break;
            case 2:
                object = vectorRoomBooking.get(rowIndex).getToDate();
                break;
            case 3:
                object = vectorRoomBooking.get(rowIndex).getNumberOfDays();
                break;
            case 4:
                object = vectorRoomBooking.get(rowIndex).getCustomerName();
                break;
            case 5:
                object = vectorRoomBooking.get(rowIndex).getCnic();
                break;
            case 6:
                object = vectorRoomBooking.get(rowIndex).getMobileNumber();
                break;
            case 7:
                object = vectorRoomBooking.get(rowIndex).getAddress();
                break;
        }
        
        return object;
    }
    
}
