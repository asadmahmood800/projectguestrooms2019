/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.FoodOrdersHistory;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author faraz
 */
public class FoodOrderHistoryTableModel extends AbstractTableModel {

    
    
    Vector<FoodOrdersHistory> vectorFoodOrdersHistory;

    public FoodOrderHistoryTableModel(Vector<FoodOrdersHistory> vectorFoodOrdersHistory) {
        this.vectorFoodOrdersHistory = vectorFoodOrdersHistory;
    }

    public FoodOrderHistoryTableModel() {
        
    }
    
    
    @Override
    public int getRowCount() {
        return vectorFoodOrdersHistory.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    
    @Override
    public String getColumnName(int columnIndex) {
        
        String columnName = "";
        
        switch(columnIndex) {
            case 0:
                columnName = "Room No";
                break;
            case 1:
                columnName = "Customer";
                break;
            case 2:
                columnName = "Mobile No";
                break;
            case 3:
                columnName = "CNIC";
                break;
            case 4:
                columnName = "Food Items";
                break;
            case 5:
                columnName = "Quantity";
                break;
            case 6:
                columnName = "Unit Price";
                break;
            case 7:
                columnName = "Total Price";
                break;
        }
        
        return columnName;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        
        Object object = null;
        
        switch (columnIndex) {
            case 0:
                object = vectorFoodOrdersHistory.get(rowIndex).getRoomNumber();
                break;
            case 1:
                object = vectorFoodOrdersHistory.get(rowIndex).getCustomerName();
                break;
            case 2:
                object = vectorFoodOrdersHistory.get(rowIndex).getMobileNumber();
                break;
            case 3:
                object = vectorFoodOrdersHistory.get(rowIndex).getCnic();
                break;
            case 4:
                object = vectorFoodOrdersHistory.get(rowIndex).getFoodItemId();
                break;
            case 5:
                object = vectorFoodOrdersHistory.get(rowIndex).getQuantity();
                break;
            case 6:
                object = vectorFoodOrdersHistory.get(rowIndex).getUnitPrice();
                break;
            case 7:
                object = vectorFoodOrdersHistory.get(rowIndex).getToatalPrice();
                break;
        }
        
        return object;
    }
    
}
