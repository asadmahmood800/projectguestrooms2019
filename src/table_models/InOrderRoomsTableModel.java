/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table_models;

import entity.Rooms;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ASAD MAHMOOD
 */
public class InOrderRoomsTableModel extends AbstractTableModel{
    private Vector<Rooms> vectorInOrderRooms;

    public InOrderRoomsTableModel(Vector<Rooms> vectorInOrderRooms) {
        this.vectorInOrderRooms = vectorInOrderRooms;
    }

    public Vector<Rooms> getVectorInOrderRooms() {
        return vectorInOrderRooms;
    }

    @Override
    public int getRowCount() {
        return vectorInOrderRooms.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String columnName = "";

        switch (columnIndex) {
            case 0:
                columnName = "ID";
                break;
            case 1:
                columnName = "Room Type";
                break;
            case 2:
                columnName = "Room Number";
                break;            
            

        }

        return columnName;

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object object = null;

        switch (columnIndex) {

            case 0:
                object = vectorInOrderRooms.get(rowIndex).getRoomId();
                break;
            case 1:
                object = vectorInOrderRooms.get(rowIndex).getRoomTypeLabel();
                break;
            case 2:
                object = vectorInOrderRooms.get(rowIndex).getRoomNumber();
                break;        
            
        }

        return object;
    }
    
}
